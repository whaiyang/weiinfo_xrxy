<?php
	/*
		Author:王海阳
		Date:13/7/12
		All rights reserved.
		http://DOMAIN.PHPSELF.?.QUERYSTR
		http://DOMAIN.REQUESTURI
	*/
	define("baseURL","localhost");
	define("DOMAIN",$_SERVER['HTTP_HOST']);
	define("PHPSELF",$_SERVER['PHP_SELF']);
	define("QUERYSTR",$_SERVER["QUERY_STRING"]);
	define("REQUESTURI",$_SERVER["REQUEST_URI"]);
	define("SCRIPTNAME",$_SERVER["SCRIPT_NAME"]);
	define("DIR",dirname(__FILE__));
	$config_arr = explode('/',$_SERVER['PHP_SELF']);
	define("URLDIR",$_SERVER['HTTP_HOST'].'/'.$config_arr[1].'/');
	
	$Dir_arr = explode("\\",DIR);
	
	//当前文件夹名
	define("FDIR",$Dir_arr[count($Dir_arr)-1]);
	//当前根URI
	define("CURI","http://".DOMAIN."/".FDIR);
	//程序入口
	define("GATE","index.php");
	//模板地址
	define("OLDTPLDIR","./webs/old/");
	//css，js等资源地址
	define("PUBDIR","./Public");
	//二级域名
	define("APP_NAME","weiinfo_xrxy");
	//weixin
	define("TOKEN","why2012");
	//wechat APPID
	define("APPID", "wx29832c98492f0e6c");
	//wechat APPSECRET
	define("APPSECRET", "0317764361e6d4728f8a3fc73df8df98");

	//豆瓣开发者信息
	define("Dou_APIKey","0e12734eae7fd8912a3759ec0b7f89b3");
	define("Dou_redirect_uri",CURI.'/'.GATE.'?action=API_DOU&sub_action=DOU_REDIRECT');
	define("Dou_secret","a2da51b718caf3b3");
	define("MAXCOUNT",5);
	//优酷开发者信息
	define("Youku_APIKEY","fce9f13bf614295c");
	define("Youku_APISecret","d6c2cc7f5f0c64364fbdedec0d682530");
	
	//php参数
	//是否开启php Open_ssl
	define("ENABLE_OPENSSL",true);
	if(ENABLE_OPENSSL)
		define("HTTPS","https");
	else
		define("HTTPS","http");
		
	//数据库
	define("LOC","localhost");//"222.197.171.171");
	define("USERNAME","xb");//"weiinfo");
	define("PASSWORD","syslabxb");//"1029384756");
	define("DATABASE","weiinfo_xrxy");
	//SQL session 存活时间 秒
	define("SESS_KEEP_TIME",7200);
	
	//表名
	define("SESSION","session");
	define("USER","user");
	define("WEIUSER","wei_user");
	define("BOOK","book");
	define("MOVIE","movie");
	define("BOOK_COMMENT","book_comment");
	define("MOVIE_SHORT_COMMENT","movie_short_comment");
	define("MOVIE_LONG_COMMENT","movie_long_comment");
	define("BOOK_AUTHOR","book_author");
	define("BOOK_TRANSLATOR","book_translator");
	define("BOOK_TAG","book_tag");
	define("MOVIE_TAG","movie_tag");
