<?php
	/**
		description:微信底层I/O接口，负责与微信服务器通信
		author:王海阳
		time:2013/4/18
	*/
	require_once("baseIO.interface.php");
	require_once("weiMSG.php");
	
	class weiIO implements IBaseIO
	{
		private $TOKEN = "";
		private $resultXML;
		
		public function __construct($token)
		{
			$this->TOKEN = $token;
		}
		
		public function open($take=false)
		{
			if($take)
			{
				date_default_timezone_set('Asia/Chongqing');
				$str = "";
				foreach($_REQUEST as $key=>$item)
				{
					$str.=$key."=".$item."		";
				}
				$str.="time:".date('Y-m-d H-i-s')."<br/>";
				@$str.="extra:".htmlspecialchars(strval($GLOBALS["HTTP_RAW_POST_DATA"]))."<br/>IP:".getIP()."<br/>...<br/>";
				file_put_contents("Manager/Debug/wei.html",$str,FILE_APPEND);
			}
			if($this->valid())
			{
				
				return true;
			}
			return false;
		}
		
		public function close()
		{
			echo $this->resultXML;
			return true;
		}
		
		public function getMSG()
		{
			@$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
			@$debug = $_POST["DEBUG"];
			if(isset($debug))
				$postStr = $debug;
			$xml = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			@$msg = weiMSGFactory::create($xml->MsgType,$xml);
			return $msg;
		}
		
		public function putMSG($msg,$take=false)
		{
			$xml = SendWeiMSG::createXML($msg);
			if($take)
			{
				$str = "";
				@$str.="Bakc:".htmlspecialchars($xml)."<br/><br/>";
				file_put_contents("Manager/Debug/wei.html",$str,FILE_APPEND);
			}
			$this->resultXML = $xml;
		}
		
		private function valid()
		{
			@$echoStr = $_GET["echostr"];
	
			if($this->checkSignature())
			{
				echo $echoStr;
				return true;
			}
			return false;
		}
		
		private function checkSignature()
		{
			$signature = $_GET["signature"];
			$timestamp = $_GET["timestamp"];
			$nonce = $_GET["nonce"];	
        		
			$token = $this->TOKEN;
			$tmpArr = array($token, $timestamp, $nonce);
			sort($tmpArr);
			$tmpStr = implode( $tmpArr );
			$tmpStr = sha1( $tmpStr );
			
			if( $tmpStr == $signature )
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function getIP()
	{
		$realip;
		if (isset($_SERVER))
		{
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
			{
				$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			else if (isset($_SERVER["HTTP_CLIENT_IP"]))
			{
				$realip = $_SERVER["HTTP_CLIENT_IP"];
			} 
			else
			{
				$realip = $_SERVER["REMOTE_ADDR"];
			}
		}
		else 
		{
			if (getenv("HTTP_X_FORWARDED_FOR"))
			{
				$realip = getenv("HTTP_X_FORWARDED_FOR");
			}
			else if (getenv("HTTP_CLIENT_IP"))
			{
				$realip = getenv("HTTP_CLIENT_IP");
			}
			else
			{
			 $realip = getenv("REMOTE_ADDR");
			}
		}
		return $realip;
	}
