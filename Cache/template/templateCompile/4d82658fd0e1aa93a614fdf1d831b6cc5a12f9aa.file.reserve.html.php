<?php /* Smarty version Smarty-3.1.17, created on 2014-04-21 22:32:10
         compiled from "webs\pages\reserve.html" */ ?>
<?php /*%%SmartyHeaderCode:18524535522fc0037c8-87620111%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d82658fd0e1aa93a614fdf1d831b6cc5a12f9aa' => 
    array (
      0 => 'webs\\pages\\reserve.html',
      1 => 1398090714,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18524535522fc0037c8-87620111',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_535522fc166b61_78446750',
  'variables' => 
  array (
    'JS' => 0,
    'PLUG' => 0,
    'CSS' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535522fc166b61_78446750')) {function content_535522fc166b61_78446750($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>
        </title>
        <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1">
        <script src="<?php echo $_smarty_tpl->tpl_vars['JS']->value;?>
/jquery-2.0.2.min.js">
        </script>
        <link href="<?php echo $_smarty_tpl->tpl_vars['PLUG']->value;?>
/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['PLUG']->value;?>
/datepicker/css/datepicker.css" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['PLUG']->value;?>
/datepicker/less/datepicker.less" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['CSS']->value;?>
/reserve.css" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['CSS']->value;?>
/echoclient.css" rel="stylesheet">
        <script src="<?php echo $_smarty_tpl->tpl_vars['PLUG']->value;?>
/dist/js/bootstrap.min.js" type="text/javascript">
        </script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['PLUG']->value;?>
/datepicker/js/bootstrap-datepicker.js" type="text/javascript">
        </script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['JS']->value;?>
/reserve.js" type="text/javascript">
        </script>
    </head>
    
    <body>
        <div id="reserve_main">
            <div class="row_border row_box" id="reserve_form">
                <div class="row_title ">
                    请认真填写表单
                </div>
                <form class="form-horizontal row_entity" role="form">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-3 col-xs-3 control-label">
                            联系人
                        </label>
                        <div class="col-sm-9 col-xs-9">
                            <input type="text" class="form-control" id="inputName" placeholder="请输入您的真实姓名">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTel" class="col-sm-3 col-xs-3 control-label">
                            联系电话
                        </label>
                        <div class="col-sm-9 col-xs-9">
                            <input type="text" class="form-control" id="inputTel" placeholder="请输入您的电话">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDate" class="col-sm-3 col-xs-3 control-label" format="yyyy-mm-dd">
                            预约日期
                        </label>
                        <div class="col-sm-9 col-xs-9">
                            <input type="text" date="1" class="form-control" id="inputDate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="areaAdder" class="col-sm-3 col-xs-3 control-label">
                            备注
                        </label>
                        <div class="col-sm-9 col-xs-9">
                            <textarea name="" class="form-control" id="areaAdder" placeholder="请输入备注信息">
                            </textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div id="submit">
                <button type="button" class="btn btn-success btn-block ">
                    提交预定
                </button>
            </div>
        </div>
    </body>
</html><?php }} ?>
