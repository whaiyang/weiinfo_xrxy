<?php
	function onSubscribe($msg,$db,$forceSend=false)
	{
		$event = "";
		if($msg->getMSGType()=="event")
			$event = $msg->getEvent();
		if($event=="subscribe"||$forceSend)
		{
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_TEXT);
			$sendMsg->setContent("电子科技大学信软学院");
			//$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_VOICE);
			//$sendMsg->setMediaId(123);
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}

	/**
		微信菜单点击事件
	*/
	function onClick($msg,$db) 
	{
		$sendMsg = null;
		$cateid = $msg->getEventKey();
		$count = $db->query("select count(*) as total from dux_category where parent_id=$cateid");
		$count = $count->fetch_assoc();
		$count = intval($count["total"]);
		if($count>0)
		{
			$cates = $db->get_all("select * from dux_category where parent_id=$cateid order by sequence asc limit 0,10");
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			foreach($cates as $key=>$value)
			{
				$pic = "";
				if(!empty($value['image']))
					$pic = getCmsPic($value['image']);
				$sendMsg->addItem(array('title'=>$value['name'],'description'=>'','picUrl'=>$pic,'url'=>setCmsUrl("xrxy","article/Category/index",array('class_id'=>$value['class_id']))));
			}
		}
		else
		{
			$count = $db->query("select count(*) as total from dux_content where class_id=$cateid");
			$count = $count->fetch_assoc();
			$count = intval($count["total"]);
			$article_num = 10;
			$more = false;
			if($count>10)
			{
				$article_num = 9;
				$more = true;
			}
			$article = $db->get_all("select * from dux_content where class_id=$cateid order by content_id desc limit 0,$article_num");
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
			foreach($article as $key=>$value)
			{
				$pic = "";
				if(!empty($value['image']))
					$pic = getCmsPic($value['image']);
				$sendMsg->addItem(array('title'=>$value['title'],'description'=>'','picUrl'=>$pic,'url'=>setCmsUrl("xrxy","article/Info/index",array('content_id'=>$value['content_id']))));
			}
			if($more)
			{
				$sendMsg->addItem(array('title'=>"查看更多",'description'=>'','picUrl'=>'','url'=>setCmsUrl("xrxy","article/Category/index",array('class_id'=>$cateid))));
			}
		}
		if(!empty($sendMsg))
		{
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}
