<?php
	class singlepageController extends Controller
	{
		public $defaultViewDir = "pages/";

		public function index()
		{
			@$aid = $_GET['aid'];
			if(empty($aid))
				return;
			$model = D("Singlepage");
			$data = $model->get_all("select * from wx_content where aid=$aid");
			if(!get_magic_quotes_gpc())
				$data[0]['content'] = stripslashes(htmlspecialchars_decode($data[0]['content']));
			else
				$data[0]['content'] = htmlspecialchars_decode($data[0]['content']);
			$this->assign("data",$data[0]);
			$this->display();
		}
	}