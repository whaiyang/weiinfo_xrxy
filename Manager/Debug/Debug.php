﻿<!DOCTYPE html>
<html>
	<head>
		<title>WEIDebug</title>
		<meta http-equiv="content-type" content="text/xml; charset=utf-8"/>
		<script type="text/javascript">
			var msgType = "text";
			
			function typeChange()
			{
				var select = document.getElementById("msgType");
				var index = select.selectedIndex;
				msgType = select.options[index].value;
				alert(msgType);
			}
		
			function onSubmit()
			{
				var xml;
				switch(msgType)
				{
					case "text":
						xml = "<xml>"+
							"<ToUserName>gh_377616861f8f</ToUserName>"+
							"<FromUserName>oeVa7jrAiUeLF1IdCVrcXKZGJTh</FromUserName>"+
							"<CreateTime>1368090779</CreateTime>"+
							"<MsgType>text</MsgType>"+
							"<Content>"+document.getElementById("content").value+"</Content>"+
							"<FuncFlag>0</FuncFlag>"+
							"</xml>";
						break;
					case "location":
						xml = "<xml>"+
							"<ToUserName>gh_377616861f8f</ToUserName>"+
							"<FromUserName>oeVa7jrAiUeLF1IdCVrcXKZGJTh</FromUserName>"+
							"<CreateTime>1368090779</CreateTime>"+
							"<MsgType>location</MsgType>"+
							"<Location_X></Location_X>"+
							"<Location_Y></Location_Y>"+
							"<Scale></Scale>"+
							"<Label>"+document.getElementById("content").value+"</Label>"+
							"<FuncFlag>0</FuncFlag>"+
							"</xml>";
						break;
					case "image":
						xml = "<xml>"+
							"<ToUserName>gh_377616861f8f</ToUserName>"+
							"<FromUserName>oeVa7jrAiUeLF1IdCVrcXKZGJTh</FromUserName>"+
							"<CreateTime>1368090779</CreateTime>"+
							"<MsgType>image</MsgType>"+
							"<PicUrl>"+document.getElementById("content").value+"</PicUrl>"+
							"<FuncFlag>0</FuncFlag>"+
							"</xml>";
						break;
					case "event_subscribe":
						xml = "<xml>"+
							"<ToUserName>gh_377616861f8f</ToUserName>"+
							"<FromUserName>oeVa7jrAiUeLF1IdCVrcXKZGJTh</FromUserName>"+
							"<CreateTime>1368090779</CreateTime>"+
							"<MsgType>event</MsgType>"+
							"<Event>"+"subscribe"+"</Event>"+
							"<EventKey>"+"subscribe"+"</EventKey>"+
							"<FuncFlag>0</FuncFlag>"+
							"</xml>";
						break;
					case "event_click":
						xml = "<xml>"+
							"<ToUserName>gh_377616861f8f</ToUserName>"+
							"<FromUserName>oeVa7jrAiUeLF1IdCVrcXKZGJTh</FromUserName>"+
							"<CreateTime>1368090779</CreateTime>"+
							"<MsgType>event</MsgType>"+
							"<Event>"+"CLICK"+"</Event>"+
							"<EventKey>"+document.getElementById("content").value+"</EventKey>"+
							"<FuncFlag>0</FuncFlag>"+
							"</xml>";
						break;
				}
				document.getElementById("DEBUG").value = xml;
				document.getElementById("form").submit();
			}
		</script>
	</head>
	<body>
		<select id="msgType" onchange="typeChange()">
			<option value="text">文本消息</option>
			<option value="event_click">菜单消息</option>
			<option value="location">地理位置消息</option>
			<option value="image">图片消息</option>
			<option value="event_subscribe">订阅</option>
		</select>
		<form id="form" method="post" action="http://<?=$_SERVER['HTTP_HOST']?>/wei/weiinfo_xrxy/index.php?action=weixin&signature=98a6bf7ff4d5006861def4405915ca0e7838b5e1&echostr=5903600603206433431&timestamp=1374075539&nonce=1374539106" enctype="text/xml">
			<input type="hidden" id="DEBUG" name="DEBUG" value="">
			<input type="text" id="content" value="你是谁">
			<input type="button" value="发送" onclick="onSubmit()"/>
		</form>
	</body>
</html>
