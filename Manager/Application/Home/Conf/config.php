<?php
	$_REQUEST_URI = explode('/',$_SERVER["REQUEST_URI"]);
	define("APPNAME",$_REQUEST_URI[1]);
	define("ROOT",'/'.APPNAME);
	define("__PUBLIC__",ROOT."/Public");
	return array(
		'SHOW_PAGE_TRACE' 	 => False, 								 // 显示页面Trace信息
		//'配置项'=>'配置值'
		'TMPL_PARSE_STRING'  =>array(
		),
		//数据库配置
		'DB_TYPE' 		     => 'mysql', 							  // 数据库类型
		'DB_HOST'            => 'localhost', 						  // 服务器地址
		'DB_NAME'   		 => 'weiinfo', 				      // 数据库名
		'DB_USER'  			 => 'root', 				 			  // 用户名
		'DB_PWD'  			 => '123', 			  					  // 密码
		'DB_PORT'  			 => 3306, 								  // 端口
		'DB_PREFIX'			 => 'wx_', 							      // 数据库表前缀
		//URL
		'URL_MODEL'          => '1', 								  //URL模式
		'SESSION_AUTO_START' => true, 								  //是否开启session
		'TOKEN_ON'=>true,  // 是否开启令牌验证
		'TOKEN_NAME'=>'__hash__',    // 令牌验证的表单隐藏字段名称
		'TOKEN_TYPE'=>'md5',  //令牌哈希验证规则 默认为MD5
		'TOKEN_RESET'=>true,  //令牌验证出错后是否重置令牌 默认为true
		//'TAGLIB_PRE_LOAD'    => 'why',
		//'APP_AUTOLOAD_PATH'  => '@.TagLib',						      //TagLib路径
		//'TAGLIB_BUILD_IN'    => 'Cx,Why',							  //并入核心库
		//默认错误跳转对应的模板文件
 		//'TMPL_ACTION_ERROR' => 'Public:success',
 		//默认成功跳转对应的模板文件
 		//'TMPL_ACTION_SUCCESS' => 'Public:success',
	);
?>