<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>
        </title>
        <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1">
        <script src="/weiInfo/Manager/Public/js/jquery-2.0.2.min.js">
        </script>
        <link href="/weiInfo/Manager/Public/plug/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="/weiInfo/Manager/Public/css/reserve.css" rel="stylesheet">
        <link href="/weiInfo/Manager/Public/css/echoclient.css" rel="stylesheet">
        <script src="/weiInfo/Manager/Public/plug/dist/js/bootstrap.min.js" type="text/javascript">
        </script>
        <script src="/weiInfo/Manager/Public/js/menu.js" type="text/javascript"></script>
    </head>
    

    <body>
        <div id="mask" class="maskPre">
            <div class="dialog_wrp simple" style="width: 726px; height: 442px; margin-left: -363px; margin-top: -221px;">
                <div class="dialog">
                    <div class="dialog_hd">
                        <h3 style="margin:5px 0px;">
                            输入提示框
                        </h3>
                        <a href="javascript:;" style="margin-top:-10px;" class="icon16_opr closed pop_closed">
                            关闭
                        </a>
                    </div>
                    <div class="dialog_bd">
                        <div class="simple_dialog_content">
                            <form id="popupForm_" method="post" class="form" onsubmit="return false;"
                            novalidate="novalidate">
                                <div class="frm_control_group">
                                    <label class="frm_label">
                                        菜单名称名字不多于4个汉字或8个字母:
                                    </label>
                                    <span class="frm_input_box">
                                        <input type="text" class="" name="popInput" value="">
                                        <input style="display:none">
                                    </span>
                                </div>
                                <div class="js_verifycode">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="dialog_ft">
                        <!--span class="btn btn_primary btn_input js_btn_p"><button class="js_btn" data-index="0">确认</button></span-->
                        <input type="button" class="js_btn btn btn-success js_btn_p" data-index="0"
                        value="确认">
                        <!--span class="btn btn_default btn_input js_btn_p"><button class="js_btn" data-index="1">取消</button></span-->
                        <input type="button" class="js_btn btn btn-success js_btn_p" data-index="1"
                        value="取消">
                    </div>
                </div>
            </div>
        </div>
        <div class="main_bd">
            <div class="parent_title_bar">
                <h3>
                    自定义菜单
                </h3>
            </div>
            <div class="menu_setting_area edit">
                <div class="menu_setting_area_hd">
                    <h3>
                        编辑
                    </h3>
                    <p>
                        可创建最多 3 个一级菜单，每个一级菜单下可创建最多 5 个二级菜单。编辑中的菜单不会马上被用户看到，请放心调试。
                    </p>
                </div>
                <div class="inner_container_box side_l menu_setting_area_bd">
                    <div class="inner_side">
                        <div class="bd">
                            <div class="menu_manage">
                                <div class="sub_title_bar light">
                                    <h4>
                                        菜单管理
                                    </h4>
                                    <div class="btn-wrp">
                                        <a href="javascript:void(0);" id="addBt" class="btn btn-success">
                                            添加
                                        </a>
                                        <a href="javascript:void(0);" id="orderBt" class="btn btn-default" style="display:none;">
                                            排序
                                        </a>
                                        <a href="javascript:void(0);" id="finishBt" class="btn btn-success btn-sorting"
                                        style="display:none">
                                            完成
                                        </a>
                                        <a href="javascript:void(0);" id="cancelBt" class="btn btn-default btn-sorting"
                                        style="display:none">
                                            取消
                                        </a>
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="inner_menu_box with_switch blue ui-sortable ui-sortable-disabled"
                                id="menuList">
                                    <dl class="inner_menu jsMenu ui-sortable ui-sortable-disabled">
                                        <dt class="inner_menu_item jslevel1" id="menu_0" data-son="p" data-type="null">
                                            <i class="icon_inner_menu_switch">
                                            </i>
                                            <a href="javascript:void(0);" class="inner_menu_link">
                                                <strong>
                                                    歌手
                                                </strong>
                                            </a>
                                            <span class="menu_opr">
                                                <a href="javascript:void(0);" class="icon14_common add_gray jsAddBt">
                                                    添加
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common  edit_gray jsEditBt">
                                                    编辑
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common del_gray jsDelBt">
                                                    删除
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt"
                                                style="display:none">
                                                    排序
                                                </a>
                                            </span>
                                        </dt>
                                    </dl>
                                    <dl class="inner_menu jsMenu ui-sortable ui-sortable-disabled">
                                        <dt class="inner_menu_item jslevel1" id="menu_1" data-son="p-s" data-type="null">
                                            <i class="icon_inner_menu_switch">
                                            </i>
                                            <a href="javascript:void(0);" class="inner_menu_link">
                                                <strong>
                                                    歌手
                                                </strong>
                                            </a>
                                            <span class="menu_opr">
                                                <a href="javascript:void(0);" class="icon14_common add_gray jsAddBt">
                                                    添加
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common  edit_gray jsEditBt">
                                                    编辑
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common del_gray jsDelBt">
                                                    删除
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt"
                                                style="display:none">
                                                    排序
                                                </a>
                                            </span>
                                        </dt>
                                        <dd class="inner_menu_item jslevel2" id="subMenu_menu_1_0" data-son="s" data-type="null">
                                            <i class="icon_dot">
                                                ●
                                            </i>
                                            <a href="javascript:void(0);" class="inner_menu_link">
                                                <strong>
                                                    欧美
                                                </strong>
                                            </a>
                                            <span class="menu_opr">
                                                <a href="javascript:void(0);" class="icon14_common edit_gray jsSubEditBt">
                                                    编辑
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common del_gray jsSubDelBt">
                                                    删除
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt"
                                                style="display:none">
                                                    排序
                                                </a>
                                            </span>
                                        </dd>
                                        <dd class="inner_menu_item jslevel2" id="subMenu_menu_1_1" data-son="s" data-type="null">
                                            <i class="icon_dot">
                                                ●
                                            </i>
                                            <a href="javascript:void(0);" class="inner_menu_link">
                                                <strong>
                                                    日韩
                                                </strong>
                                            </a>
                                            <span class="menu_opr">
                                                <a href="javascript:void(0);" class="icon14_common edit_gray jsSubEditBt">
                                                    编辑
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common del_gray jsSubDelBt">
                                                    删除
                                                </a>
                                                <a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt"
                                                style="display:none">
                                                    排序
                                                </a>
                                            </span>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inner_main">
                        <div class="bd">
                            <div class="action_setting">
                                <div class="sub_title_bar light">
                                    <h4>
                                        设置动作
                                    </h4>
                                    <div class="btn-wrp">
                                        <a href="javascript:void(0);" class="btn btn-default btn-editing" id="changeBt"
                                        style="display: none;">
                                            修改
                                        </a>
                                    </div>
                                </div>
                                <div class="action_content default alreadySon jsMain" id="alreadySon" style="display:none">
                                    <p class="action_tips">
                                        已有子菜单，无法设置动作
                                    </p>
                                </div>
                                <div class="action_content default jsMain" id="none" style="display:none">
                                    <p class="action_tips">
                                        你可以先添加一个菜单，然后开始为其设置响应动作
                                    </p>
                                </div>
                                <div class="action_content init jsMain" style="display: none;" id="index">
                                    <p class="action_tips">
                                        请选择订阅者点击菜单后，公众号做出的相应动作
                                    </p>
                                    <a href="javascript:void(0);" id="sendMsg">
                                        <i class="icon_menu_action send">
                                        </i>
                                        <strong>
                                            设置key
                                        </strong>
                                    </a>
                                    <a href="javascript:void(0);" id="goPage">
                                        <i class="icon_menu_action url">
                                        </i>
                                        <strong>
                                            跳转到网页
                                        </strong>
                                    </a>
                                </div>
                                <div class="action_content url jsMain" id="url" style="display:none;">
                                    <form action="" id="urlForm" onsubmit="return false;">
                                        <p class="action_tips">
                                            订阅者点击该子菜单会跳到以下链接
                                        </p>
                                        <div class="frm_control_group">
                                            <span class="frm_input_box">
                                                <input type="text" class="" id="urlText" name="urlText">
                                            </span>
                                            <p class="frm_msg fail" style="display: none;" id="urlFail">
                                                <i>
                                                    ●
                                                </i>
                                                <span for="urlText" class="frm_msg_content" style="display: inline;">
                                                    请输入正确的URL
                                                </span>
                                            </p>
                                        </div>
                                    </form>
                                    <div class="tool_bar">
                                        <a href="javascript:void(0);" class="btn btn-default" id="urlBack">
                                            返回
                                        </a>
                                        <a class="submit btn btn-success" type="submit" id="urlSave">
                                            保存
                                        </a>
                                    </div>
                                </div>
                                <div class="action_content addSub default jsMain" id="addSub" style="display:none;">
                                    <p class="action_tips">
                                        使用二级菜单后,当前编辑的消息将会被清除。确定使用二级菜单?
                                    </p>
                                    <div class="tool_bar" style="width:115px; margin: 0 auto;">
                                        <a href="javascript:void(0);" class="btn btn-default" id="urlBack">
                                            返回
                                        </a>
                                        <a class="submit btn btn-success" type="submit" id="urlSave">
                                            确定
                                        </a>
                                    </div>
                                </div>
                                <div class="action_content sended default jsMain" id="view" style="display: none;">
                                    <p class="action_tips">
                                        订阅者点击该子菜单会收到以下消息
                                    </p>
                                    <div class="msg_wrp" id="viewDiv">
                                    </div>
                                </div>
                                <div class="action_content send default jsMain" id="edit" style="display: none;">
                                    <p class="action_tips">
                                        KEY
                                    </p>
                                    <div class="msg_sender small" id="editDiv" style="width:115px; margin:10px auto;">
                                        <input type="text" name="message" class="">
                                    </div>
                                    <div class="tool_bar" style="width:115px; margin: 0 auto;">
                                        <a href="javascript:void(0);" class="btn btn-default" id="editBack">
                                            返回
                                        </a>
                                        <a href="javascript:void(0);" class="btn btn-success" id="editSave">
                                            保存
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu_setting_area release">
                <div class="menu_setting_area_hd">
                    <h3>
                        发布
                    </h3>
                    <p>
                        编辑中的菜单不能直接在用户手机上生效，你需要进行发布，发布后24小时内所有的用户都将更新到新的菜单。
                    </p>
                </div>
                <div class="menu_setting_area_bd">
                    <div class="tool_bar border tc">
                        <!-- <a href="javascript:void(0);" class="btn btn-success" id="viewBt">
                            预览
                        </a> -->
                        <a href="javascript:void(0);" class="btn btn-success" id="pubBt">
                            发布
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>

        <script src="/weiInfo/Manager/Public/js/reserve.js" type="text/javascript">
        </script>
</html>