<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>
        </title>
        <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1">
        <script src="/wei/weiinfo_xrxy/Manager/Public/js/jquery-2.0.2.min.js">
        </script>
        <link href="/wei/weiinfo_xrxy/Manager/Public/plug/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="/wei/weiinfo_xrxy/Manager/Public/css/reserve.css" rel="stylesheet">
        <link href="/wei/weiinfo_xrxy/Manager/Public/css/echoclient.css" rel="stylesheet">
        <script src="/wei/weiinfo_xrxy/Manager/Public/plug/dist/js/bootstrap.min.js" type="text/javascript">
        </script>
        <script src="/wei/weiinfo_xrxy/Manager/Public/js/reserve.js" type="text/javascript">
        </script>
    </head>
    
    <body>
       <h2>HEADER</h2>
        <div id="lib_main">
            <div id="lib_sidebar">
                <div class="lib_switch" data="user">
                    <h5>
                        <span class="lib_arrow">
                        </span>
                        用户管理
                    </h5>
                    <div class="lib_body" style="display: none;">
                        <ul>
                            <li data="user_info" data-target='<?php echo U("User/index");?>'>
                                用户信息
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="lib_switch" data="act">
                    <h5>
                        <span class="lib_arrow">
                        </span>
                        活动管理
                    </h5>
                    <div class="lib_body" style="display: none;">
                        <ul>
                            <li data="act_prize" data-target='<?php echo U("Activity/prize");?>'>
                                中奖信息管理
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="lib_switch" data="api">
                    <h5>
                        <span class="lib_arrow">
                        </span>
                        微信接口
                    </h5>
                    <div class="lib_body" style="display: none;">
                        <ul>
                            <li data="api_vidio" data-target='<?php echo U("Weixin/video");?>'>
                                视频上传
                            </li>
                            <li data="api_vidio" data-target='<?php echo U("WeiMenu/index");?>'>
                                自定义菜单
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="lib_switch" data="weiPage">
                    <h5>
                        <span class="lib_arrow">
                        </span>
                        微页面管理
                    </h5>
                    <div class="lib_body" style="display: none;">
                        <ul>
                            <li data="api_vidio" data-target='<?php echo U("SPage/view",array("type"=>0,"cat"=>1));?>'>
                                查看
                            </li>
                            <li data="api_vidio" data-target='<?php echo U("SPage/add");?>'>
                                添加
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="lib_page">
                <div class="container">
                    <iframe src="<?php echo U("User/index");?>" frameborder="0" height="100%" width="100%"></iframe>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <h2>FOOTER</h2>
    </body>
</html>