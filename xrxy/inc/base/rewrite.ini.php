<?php 
//伪静态规则
$config['REWRITE']['list-<class_id>-p-<page>.html'] = 'article/Category/index';
$config['REWRITE']['list-<class_id>.html'] = 'article/Category/index';
$config['REWRITE']['page-<class_id>-p-<page>.html'] = 'pages/Category/index';
$config['REWRITE']['page-<class_id>.html'] = 'pages/Category/index';
$config['REWRITE']['form-<name>-page-<page>.html'] = 'duxcms/Form/index';
$config['REWRITE']['form-<name>.html'] = 'duxcms/Form/index';
$config['REWRITE']['tags-list-page-<page>.html'] = 'duxcms/Tags/tagList';
$config['REWRITE']['tags-index.html'] = 'duxcms/Tags/tagList';
$config['REWRITE']['tags-<name>-page-<page>.html'] = 'duxcms/Tags/index';
$config['REWRITE']['tags-<name>.html'] = 'duxcms/Tags/index';
$config['REWRITE']['info-<class_id>-<content_id>-page-<page>.html'] = 'article/Info/index';
$config['REWRITE']['info-<class_id>-<content_id>.html'] = 'article/Info/index';
