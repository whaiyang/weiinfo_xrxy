<?php
class SiteController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    //模板显示
    protected function show($tpl = '', $return = false, $is_tpl = true){
        if( $is_tpl ){
            $tpl = empty($tpl) ? CONTROLLER_NAME . '/'. ACTION_NAME : $tpl;
            if( $is_tpl && $this->layout ){
                $this->__template_file = $tpl;
                $tpl = $this->layout;
            }
            $tpl_array=explode('.', $tpl);
            $ext='.'.end($tpl_array);
            $name=substr($tpl,0,-intval(strlen($ext)));
        }
        $config=config('TPL');
        $tpl_dir=ROOT_PATH.$config['TPL_TEMPLATE_PATH'].$this->siteConfig['tpl_dir'].'/';
        if(!file_exists($tpl_dir.$tpl)&&$tpl<>'404.html'){
            $appconfig=config('APP');
            if(!$appconfig['DEBUG']){
                $this->error404();
            }
        }
        $this->view()->config['TPL_TEMPLATE_PATH'] = $tpl_dir;
        $this->view()->config['TPL_TEMPLATE_SUFFIX'] = $ext;
        $this->assign('config', config());
        $this->assign('siteInfo', $this->siteConfig);
        return $this->view()->display($name, $return, $is_tpl);
    }

    protected function error404(){
        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');
        $this->show('404.html');
        exit;
    }

    //分页结果显示
    protected function pageShow($count)
    {
        $this->pager['obj']->nextPage=$this->siteConfig['page_next'];
        $this->pager['obj']->prePage=$this->siteConfig['page_pre'];
        $this->pager['obj']->firstPage=$this->siteConfig['page_first'];
        $this->pager['obj']->lastPage=$this->siteConfig['page_last'];
        return $this->pager['obj']->show($this->pager['url'], $count, $this->pager['num'],$this->siteConfig['page_num'],$this->siteConfig['page_show']);
    }
}