<?php
class UserController extends BaseController
{
    protected $appID = 'user';
    public function __construct()
    {
        $this->appID = empty($appID) ? $this->appID : $appID;
        $config = appConfig('member');
        $this->userConfig = $config['APP_SETTING'];
        $this->site();
        parent::__construct();
        $this->checkLogin();
    }
    //页面不存在
    protected function error404(){
        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');
        $this->showFrame('404','member');
        exit;
    }
    //站点信息
    public function site()
    {
        $info = api('duxcms','setSiteConfig');
        $this->siteConfig=$info['data'];
        $_POST['site']=$info['siteId'];
        defined('SITEID') or define('SITEID', $info['siteId']);
        defined('OLDSITEID') or define('OLDSITEID', $info['data']['site_id']);
    }
    //模板显示含公共
    protected function showFrame($tpl = '', $app = '')
    {
        $content = $this->display($tpl, true, true, $app);
        $layout = $this->display('commonframe', true, true, 'member');
        echo str_replace('<!--common-->', $content, $layout);
    }
    //模板显示含公共
    protected function show($tpl = '', $app = '')
    {
        
        $this->menuList = api('member','getMenuList',array('purview'=>true,'user_id'=>$this->userId));
        $content = $this->display($tpl, true, true, $app);
        $layout = $this->display('common', true, true, 'member');
        echo str_replace('<!--common-->', $content, $layout);
    }
    //权限检测
    protected function checkPurview()
    {
        $basePurview = $this->memberInfo['base_purview'];
        $this->basePurview = $basePurview;

        $purviewInfo = api(APP_NAME, 'apiMemberPurview');
        if (empty($purviewInfo)) {
            return true;
        }
        $controller = $purviewInfo[CONTROLLER_NAME];
        if (empty($controller['auth'])) {
            return true;
        }
        $action = $controller['auth'][ACTION_NAME];
        if (empty($action)) {
            return true;
        }
        $current = APP_NAME . '_' . CONTROLLER_NAME;
        if (!in_array($current, (array) $basePurview)) {
            $this->msg('您没有权限访问此功能！', 0);
        }
        $current = APP_NAME . '_' . CONTROLLER_NAME . '_' . ACTION_NAME;
        if (!in_array($current, (array) $basePurview)) {
            $this->msg('您没有权限访问此功能！', 0);
        }
        return true;
    }
    //登录检测
    protected function checkLogin()
    {
        //不需要登录验证的页面
        $noLogin = api(APP_NAME,'apiMemberLoginExclude');
        //如果当前访问是无需登录验证，则直接返回
        if (isset($noLogin[APP_NAME]) && isset($noLogin[APP_NAME][CONTROLLER_NAME]) && in_array(ACTION_NAME, $noLogin[APP_NAME][CONTROLLER_NAME])) {
            return true;
        }
        //没有登录,则跳转到登录页面
        if (!$this->isLogin()) {
            $this->redirect(url('member/Login/index'));
        }
        $this->checkPurview();
        return true;
    }
    //获取登录
    protected function getLogin()
    {
        return $this->userId;
    }
    //判断是否登录
    protected function isLogin()
    {
        $userId = intval(get_cookie($this->appID . '_uid'));
        if (empty($userId)) {
            return false;
        } else {
            $this->userId = $userId;
            $this->memberInfo = api('member', 'getMemberInfo', array('user_id'=>$userId));
            return true;
        }
    }
    //设置登录
    protected function setLogin($userInfo, $time = 0)
    {
        return set_cookie($this->appID . '_uid', $userInfo, '/', $time);
    }
    //退出
    protected function clearLogin()
    {
        set_cookie($this->appID . '_uid', '', '/', -1);
        $this->redirect(url('member/Login/index'));
    }

}