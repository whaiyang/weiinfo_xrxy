<?php
/**
*  description: encapsulate part of wechat API
*  1. get access token
*  2. http GET & POST
*  @author: Bill Xue
*  @date: 2014/4/20
*/

class wechat{
	const API_URL_PREFIX = 'https://api.weixin.qq.com/cgi-bin';
	const AUTH_URL = '/token?grant_type=client_credential&';
	const MENU_CREATE_URL = '/menu/create?';
	const MENU_DELETE_URL = '/menu/delete?';
	const MENU_GET_URL = '/menu/get?';
	const ACC_TOKEN_CACHE = 'weilib/acc_token_cache.txt';
	const ACC_TOKEN_EXPIRE = 7200;


	private $access_token;
	private $appid;
	private $appsecret;
	public $errCode;
	public $errMsg;
	/**
	 * GET query
	 * @param string $url
	 */
	public function __construct($id, $secret){
		$this->appid = isset($id)?$id:'';
		$this->appsecret = isset($secret)?$secret:'';
	}
	private function http_get($url){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
	
	/**
	 * POST query
	 * @param string $url
	 * @param string $param
	 * @return string content
	 */
	private function http_post($url,$data){
		$MENU_URL=$url;

		$ch = curl_init(); 

		curl_setopt($ch, CURLOPT_URL, $MENU_URL); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

		$info = curl_exec($ch);

		if (curl_errno($ch)) {
    		echo 'Errno'.curl_error($ch);
		}

		curl_close($ch);

		return $info;
	}
	private function saveAccessToken($time, $token){
		if (!is_writable(self::ACC_TOKEN_CACHE)){
			exit("Unable to write access_token cache!");
		}

		$file = fopen(self::ACC_TOKEN_CACHE, "w");
		fwrite($file, $time.PHP_EOL);
		fwrite($file, $token);
		fclose($file);
	}
	
	/**
	 * get access_token
	 * @param string $appid
	 * @param string $appsecret
	 * no care able token experiation, so query it every time 
	 */
	public function getAccessToken($appid='',$appsecret=''){
		if (!$appid || !$appsecret) {
			$appid = $this->appid;
			$appsecret = $this->appsecret;
		}
		$file = fopen(self::ACC_TOKEN_CACHE, "r") or exit("Unable to open file!");
		$i = 0;
		$last_data = array();
		while (!feof($file)){
			$last_data[$i] = fgets($file);
			$i++;
		}
		fclose($file);
		if (($i == 0) || (time() >= $last_data[0]+self::ACC_TOKEN_EXPIRE-600)){
			// no last access_token record or time out
			$result = $this->http_get(self::API_URL_PREFIX.self::AUTH_URL.'appid='.$appid.'&secret='.$appsecret);
			if ($result)
			{
				$json = json_decode($result,true);
				if (!$json || isset($json['errcode'])) {
					$this->errCode = $json['errcode'];
					$this->errMsg = $json['errmsg'];
					return false;
				}
				$this->access_token = $json['access_token'];
				$time = time();
				$this->saveAccessToken($time, $this->access_token);
				return $this->access_token;
			}
			return false;
		}
		else{
			$this->access_token = $last_data[1];
			return $this->access_token;
		}
		return false;
	}

	/**
	 * reset access_token
	 * @param string $appid
	 */
	public function resetAccessToken($appid=''){
		$this->access_token = '';
		//TODO: remove cache
		return true;
	}
	/**
	 * create menu
	 * @param json_string $data
	 */
	public function createMenu($data){
		if (!$this->access_token && !$this->getAccessToken()) return false;
		$result = $this->http_post(self::API_URL_PREFIX.self::MENU_CREATE_URL.'access_token='.$this->access_token,$data);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return true;
		}
		return false;
	}
	/**
	 * get menu
	 * @return json $json
	 */
	public function getMenu(){
		if (!$this->access_token && !$this->checkAuth()) return false;
		$result = $this->http_get(self::API_URL_PREFIX.self::MENU_GET_URL.'access_token='.$this->access_token);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || isset($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
	}
	/**
	 * delete menu
	 */
	public function deleteMenu(){
		if (!$this->access_token && !$this->checkAuth()) return false;
		$result = $this->http_get(self::API_URL_PREFIX.self::MENU_DELETE_URL.'access_token='.$this->access_token);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return true;
		}
		return false;
	}
}
?>