<?php
/**
 * StatisticsModel.php
 * 数据统计
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class StatisticsModel extends BaseModel
{
    /**
     * 菜单表数量
     */
    public function getCatalog()
    {
        return $this->model->table('catalog')->where('site = '.SITEID)->count();
    }
    /**
     * 内容表数量
     */
    public function getContent()
    {
        return $this->model->table('content')->where('site = '.SITEID)->count();
    }
    /**
     * TAG表数量
     */
    public function getTag()
    {
        return $this->model->table('tags')->where('site = '.SITEID)->count();
    }
    /**
     * 栏目表数量
     */
    public function getCategory()
    {
        return $this->model->table('category')->where('site = '.SITEID)->count();
    }
}