<?php
/**
 * CacheModel.php
 * 缓存管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class CacheModel extends BaseModel
{
    /**
     * 获取缓存列表
     * @return array 缓存列表
     */
    public function loadData()
    {
        $list = array(
            'api'=>array(
                'name'=>'API缓存',
                'dir'=>'cache/app_cache/app_list.php',
                ),
            'menu'=>array(
                'name'=>'系统菜单缓存',
                'dir'=>'cache/app_cache/admin_menu.php',
                ),
            'tpl'=>array(
                'name'=>'模板缓存',
                'dir'=>'cache/tpl_cache',
                ),
            'html'=>array(
                'name'=>'静态缓存',
                'dir'=>'cache/html_cache',
                ),
            'data'=>array(
                'name'=>'数据库缓存',
                'dir'=>'cache/db_cache/cachedata.php',
                ),
            'session'=>array(
                'name'=>'SESSION缓存',
                'dir'=>'cache/session_cache/cachedata.php',
                ),
            );
        $apiList = hook_api('apiCacheList');
        $newApiList = array();
        if(!empty($apiList)){
            foreach ($apiList as $key => $value) {
                if(!empty($value)){
                    foreach ($value as $k => $v) {
                        $newApiList[$k] = $v;
                    }
                }
                
            }
        }
        $list = array_merge($list,$newApiList);
        return $list;
    }
    /**
     * 清除缓存
     * @param int $type 类型
     * @return bool 状态
     */
    public function clearCache($type)
    {
        if(empty($type)){
            return false;
        }
        $type = in($type);
        $cacheList = model('Cache')->loadData();
        $info = $cacheList[$type];
        $file = realpath(ROOT_PATH.'/'.$info['dir']);
        if(!file_exists($file)){
            return false;
        }
        if (is_dir($file)) {
            return @del_dir($file);
        } else if (is_file($file)) {
            return @unlink($file);
        }
        return false;
    }
}