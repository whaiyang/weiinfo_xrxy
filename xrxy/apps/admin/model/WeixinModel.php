<?php
	class WeixinModel extends BaseModel
	{
		protected $table = 'catalog';

		public function getMenu()
		{
			$dataParent = $this->model->query("select cata.catalog_id,cata.parent_id,cata.url,c.name from dux_catalog cata,dux_category c where cata.parent_id=0 AND cata.class_id=c.class_id order by cata.sequence asc");
			$dataSub = array();
			foreach($dataParent as $value)
			{
				$dataSub[] = $this->model->query("select cata.catalog_id,cata.parent_id,cata.url,c.name from dux_catalog cata,dux_category c where cata.show=1 AND cata.class_id=c.class_id AND cata.parent_id=".$value['catalog_id']." order by cata.sequence asc");
			}
			return array("parent"=>$dataParent,"sub"=>$dataSub);
		}
	}