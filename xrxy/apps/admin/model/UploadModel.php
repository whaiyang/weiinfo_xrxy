<?php
/**
 * UploadModel.php
 * 上传数据处理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class UploadModel extends BaseModel
{
    /**
     * 上传数据
     * @param array $files 上传$_FILES信息
     * @param array $data 上传配置信息可选
     * @return array 文件信息
     */
    public function uploadData($files, $data = array())
    {
        $relationKey = in($_POST['relation_key']);
        if (empty($relationKey)) {
            return '文件关联KEY不能为空！';
        }
        if (empty($files)) {
            return '上传文件不能为空！';
        }
        $banExt = array(
            'php',
            'asp',
            'asp',
            'html',
            'htm',
            'js',
            'shtml',
            'txt',
            'aspx'
        );
        foreach ($files as $file) {
            $name = $file['name'];
            $ext = explode('.', $file['name']);
            $ext = strtolower(end($ext));
            if (in_array($ext, $banExt)) {
                return '非法文件上传！';
            }
        }
        //文件路径
        $dirPath = ROOT_PATH . 'upload/';
        $dirUrl = __ROOT__ . '/upload/';
        $fileTime = date('Y-m') . '/' . date('d');
        //上传
        $config = config('UPLOAD');
        $config = array_merge((array)$config, (array)$data);
        $upload = new UploadFile();
        $upload->maxSize = 1024 * 1024 * $config['UPLOAD_SIZE'];
        $upload->allowExts = explode(',', $config['UPLOAD_TYPE']);
        $upload->savePath = $dirPath . $fileTime . '/';
        $upload->saveRule = cp_uniqid;
        if (!$upload->upload()) {
            return $upload->getErrorMsg();
        }
        //上传信息
        $info = $upload->getUploadFileInfo();
        $info = $info[0];
        $ext = strtolower($info['extension']);
        $imageType = array(
            'jpg',
            'jpeg',
            'png',
            'gif'
        );
        $filePath = $dirPath . $fileTime . '/' . $info['savename'];
        $fileUrl = $dirUrl . $fileTime . '/' . $info['savename'];
        $fileName = substr($info['savename'], 0, -(intval(strlen($ext)) + 1));
        $file = $fileUrl;
        //处理图片数据
        if (in_array($ext, $imageType)) {
            //处理水印
            if ($config['WATERMARK_ON']) {
                $waterFile = ROOT_PATH . '/public/watermark/' . $config['WATERMARK_IMAGE'];
                if (file_exists($waterFile)) {
                    Image::water($filePath, $waterFile, $config['WATERMARK_POSITION']);
                }
            }
            //处理缩略图
            if ($config['THUMBNAIL_ON']) {
                $thumbFile = $fileTime . '/' . $fileName . '_thumb.' . $ext;
                $thumb = Image::thumb($filePath, $dirPath . $thumbFile, '', $config['THUMBNAIL_WIDTH'], $config['THUMBNAIL_HEIGHT'], '', $config['THUMBNAIL_TYPE']);
                if ($thumb) {
                    $file = $dirUrl . $thumbFile;
                }
            }
        }
        $title = substr($name, 0, -(strlen($ext) + 1));
        //设置返回数组
        $info = array(
            'url' => $file,
            'original' => $fileUrl,
            'title' => $title,
            'ext' => $ext,
            'time' => time(),
            'size' => $info['size'],
            'relation_key' => $relationKey
        );
        $info=model('Attachment')->addData($info);
        //上传API
        hook_api('ApiUploadData',$info);
        return $info;
    }
}