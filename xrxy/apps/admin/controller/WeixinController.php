<?php
	/*
		WeixinController
		WeixinModel
		Wexin.js
	*/
	class WeixinController extends AdminController
	{
		private $APPID = "wxa11e7dcdee2317db";
		private $APPSECRET = "49fa30eb0486db4cc2ab882e7f7c29c3";

		public function refreshMenu()
		{
			require_once("weilib/wechat.php");
			$WX = model("Weixin");
			$menuData = $WX->getMenu();
			$parent = $menuData['parent'];
			$sub = $menuData['sub'];
			$jsonMenu = array("button"=>array());
			foreach($parent as $key=>$value)
			{
				$value['name'] = urlencode($value['name']);
				$jsonMenu['button'][$key] = array("type"=>"click","name"=>$value['name'],"key"=>$value['catalog_id']);
				$submenu = $sub[$key];
				if(!empty($submenu))
					unset($jsonMenu['button'][$key]['key']);
				foreach($submenu as $value2)
				{
					$value2['name'] = urlencode($value2['name']);
					$subArr = array();
					if(empty($value2['url']))
					{
						$subArr = array("type"=>"click","name"=>$value2['name'],"key"=>$value2['catalog_id']);
					}
					else
					{
						$value2['url'] = str_replace("&amp;","&", $value2['url']);
						$subArr = array("type"=>"view","name"=>$value2['name'],"url"=>$value2['url']);
					}
					$jsonMenu['button'][$key]["sub_button"][] = $subArr;
				}
			}
			$jsonMenu = json_encode($jsonMenu);
			$jsonMenu = urldecode($jsonMenu);
			//var_dump($jsonMenu);
			$wechat = new wechat($this->APPID, $this->APPSECRET);
			$msg = $wechat->getAccessToken();
			if ($wechat->createMenu($jsonMenu))
			{
				echo "create menu OK.";
			}
			else
			{
				echo "create menu fail.".$wechat->errMsg;
			}
		}
	}