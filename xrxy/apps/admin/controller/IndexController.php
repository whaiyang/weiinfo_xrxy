<?php
/**
 * IndexController.php
 * 系统主页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class IndexController extends AdminController
{
    /**
     * 主框架页面
     */
    public function index()
    {
        $this->assign('siteInfo', api('duxcms','getSiteInfo',OLDSITEID));
        $this->assign('siteList', api('duxcms','getSiteList'));
        $this->assign('userInfo', $this->userInfo);
        $this->assign('adminInfo', config('ADMIN'));
        $this->display();
    }
    /**
     * 欢迎页面
     */
    public function home()
    {
        //统计表
        $catalogNum=model('Statistics')->getCatalog();
        $contentNum=model('Statistics')->getContent();
        $tagNum=model('Statistics')->getTag();
        $categoryNum=model('Statistics')->getCategory();
        //站点信息
        $this->assign('siteInfo', api('duxcms','getSiteInfo',OLDSITEID));
        $this->assign('verInfo', config('COPYRIGHT'));
        $this->assign('catalogNum', $catalogNum);
        $this->assign('contentNum', $contentNum);
        $this->assign('tagNum', $tagNum);
        $this->assign('categoryNum', $categoryNum);
        $this->show();
    }
    /**
     * 清除BOM
     */
    public function sysbom(){
        @model('Bom')->tool_bom_dir(ROOT_PATH.'inc');
        @model('Bom')->tool_bom_dir(ROOT_PATH.'themes');
        $this->msg('所有BOM清除完毕！');
    }
}