<?php
/**
 * ContentUploadController.php
 * 文章上传页面
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class ContentUploadController extends AdminController
{
    /**
     * AJAX文章上传请求地址
     */
    public function index()
    {
        $classId=intval($_POST['class_id']);
        if(empty($classId)){
            $this->uploadTips('请先选择栏目！',false);
        }
        //获取栏目信息
        $categoryInfo=model('CategoryArticle')->getInfo($classId);
        if(empty($categoryInfo)){
            $this->uploadTips('没有发现栏目！',false);
        }
        //处理配置文件
        $config=unserialize($categoryInfo['config_upload']);
        if($config['THUMBNAIL_TYPE_INHERIT']){
            unset($config['THUMBNAIL_TYPE']);
        }
        if($config['THUMBNAIL_ON_INHERIT']){
            unset($config['THUMBNAIL_ON']);
        }
        if($config['THUMBNAIL_SIZE_INHERIT']){
            unset($config['THUMBNAIL_WIDTH']);
            unset($config['THUMBNAIL_HEIGHT']);
        }
        if($config['WATERMARK_ON_INHERIT']){
            unset($config['WATERMARK_ON']);
        }
        if($config['WATERMARK_POSITION_INHERIT']){
            unset($config['WATERMARK_POSITION']);
        }
        if($config['WATERMARK_IMAGE_INHERIT']){
            unset($config['WATERMARK_IMAGE']);
        }
        if($config['UPLOAD_TYPE_INHERIT']){
            unset($config['UPLOAD_TYPE']);
        }
        if($config['UPLOAD_SIZE_INHERIT']){
            unset($config['UPLOAD_SIZE']);
        }
        $info=api('admin','addUpload',array('files'=>$_FILES,'config'=>$config));
        if (is_array($info)) {
            $this->uploadTips($info);
        } else {
            $this->uploadTips($info,false);
        }
    }
    public function uploadTips($info,$status=true)
    {
        header('Content-type: text/html; charset=UTF-8');
        if(!$status){
            echo json_encode(array(
                'error' => 1,
                'message' => $info
            ));
        }else{
            echo json_encode(array(
                'error' => 0,
                'url' => $info['url'],
                'info' => $info
            ));
        }
        exit;
    }
}