<?php
/**
 * AdminModelController.php
 * 模型预设管理
 * @author Life <349865361@qq.com>
 * @version 20140226
 */
class AdminModelController extends AdminController
{
    /**
     * 编辑
     */
    public function edit()
    {
        $config = appConfig('article');
        $info = $config['APP_MODEL_CONFIG'];
        //模板赋值
        $this->assign('info', $info);
        $this->assign('FieldsetList', api('duxcms','loadFieldsetData'));
        $this->show('adminmodel/info');
    }
    /**
     * 处理
     */
    public function editData()
    {
        unset($_POST['site']);
        unset($_POST['relation_key']);
        $data['APP_MODEL_CONFIG']=in($_POST);
        if( save_config('article', $data)){
            $this->msg('模型设置成功！');
        }else{
            $this->msg('模型设置失败，APP目录无写入权限！');
        }
    }
}