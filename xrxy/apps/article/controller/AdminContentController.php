<?php
/**
 * AdminContentController.php
 * 内容模型操作
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminContentController extends AdminController
{
    /**
     * 文章列表
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterClassId = intval($_GET['class_id']);
        $filterStatus = intval($_GET['status']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND A.title LIKE "%' . $filterKeyword . '%"';
        }
        if(!empty($filterClassId)) {
            $filterWhere .= ' AND B.class_id='.$filterClassId;
        }
        if(!empty($filterStatus)) {
            if($filterStatus==1){
                $filterWhere .= ' AND A.status=1';
            }else{
                $filterWhere .= ' AND A.status=0';
            }
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword,
            'content_id' => $filtercontentId,
            'status' => $filterStatus,
        );
        $url = url('AdminContent/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'B.app="'.APP_NAME.'"'.$filterWhere;
        //列表信息
        $list = api('duxcms','loadContentData',array('where'=>$where,'limit'=>$limit));
        $count = api('duxcms','countContentData',array('where'=>$where));
        $appConfig=config('APP');
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['aurl']=model('ContentData')->getUrl($value, $appConfig);
            }
        }
        $list = $data;
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('categoryList', model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0));
        $this->assign('positionList', api('duxcms','loadPositionList'));
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加内容
     */
    public function add()
    {
        $appConfig = appConfig('article');
        $info = $appConfig['APP_MODEL_CONFIG'];
        $this->assign('info', $info);
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('categoryList', model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0));
        $this->show('AdminContent/info');
    }
    /**
     * 处理内容添加
     */
    public function addData()
    {
        if (empty($_POST['class_id'])) {
            $this->msg('请选择分类ID！', false);
        }

        if (empty($_POST['title'])) {
            $this->msg('请填写文章标题！', false);
        }
        //检测扩展字段
        $checkFieldset=api('duxcms','checkFieldsetData',$_POST);
        if(!empty($checkFieldset)){
            $this->msg($checkFieldset, false);
        }
        
        //保存内容信息
        $_POST['content_id']=api('duxcms','addContentData',$_POST);
        model('ContentArticle')->addData($_POST);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$_POST['content_id'],'model'=>'content_atricle','relation_key'=>$_POST['relation_key']));
        $this->msg('文章添加成功！');
    }
    /**
     * 编辑内容
     */
    public function edit()
    {
        $contentId = intval($_GET['content_id']);
        if (empty($contentId)) {
            $this->msg('无法获取内容ID！', false);
        }
        //内容信息
        $info = api('duxcms', 'getContentData',array('content_id'=>$contentId));
        $extInfo = model('ContentArticle')->getInfo($contentId);
        $info = array_merge((array)$info,(array)$extInfo);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '编辑');
        $this->assign('categoryList', model('CategoryArticle')->getTrueData('A.app="'.APP_NAME.'"', 0));
        $this->show('AdminContent/info');
    }
    /**
     * 处理内容
     */
    public function editData()
    {
        if (empty($_POST['content_id'])) {
            $this->msg('无法获取内容信息！', false);
        }
        if (empty($_POST['class_id'])) {
            $this->msg('请选择分类ID！', false);
        }
        if (empty($_POST['title'])) {
            $this->msg('请填写文章标题！', false);
        }
        //检测扩展字段
        $checkFieldset=api('duxcms','checkFieldsetData',$_POST);
        if(!empty($checkFieldset)){
            $this->msg($checkFieldset, false);
        }
        //保存内容信息
        api('duxcms','saveContentData',$_POST);
        model('ContentArticle')->saveData($_POST);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$_POST['content_id'],'model'=>'content_atricle','relation_key'=>$_POST['relation_key']));
        $this->msg('文章修改成功！');
    }
    /**
     * 删除内容
     */
    public function del()
    {
        $contentId=intval($_POST['data']);
        if (empty($contentId)) {
            $this->msg('无法获取内容ID！', false);
        }
        api('duxcms', 'delContentData',array('content_id'=>$contentId));
        model('ContentArticle')->delData($contentId);
        //关联附件
        api('admin','delRelationFile',array('relation_id'=>$contentId,'model'=>'content_atricle'));
        $this->msg('内容删除成功！',1);
    }
    /*批量操作*/
    public function batch()
    {
        $contentId=implode(',',$_POST['id']);
        if(empty($contentId)){
            $this->alert('请先选择内容！');
        }
        $type=intval($_POST['type']);
        if(empty($type)){
            $this->alert('请先选择操作！');
        }
        switch ($type) {
            case 1:
                //发布
                $data=array();
                $data['content_id']=$contentId;
                $data['status']=1;
                api('duxcms','saveContentData',$data);
                break;
            case 2:
                //草稿
                $data=array();
                $data['content_id']=$contentId;
                $data['status']=0;
                api('duxcms','saveContentData',$data);
                break;
            case 3:
                //删除
                $list = explode(',', $contentId);
                if(!empty($list)){
                    foreach ($list as $value) {
                        api('duxcms', 'delContentData',array('content_id'=>$value));
                        model('ContentArticle')->delData($value);
                        //关联附件
                        api('admin','delRelationFile',array('relation_id'=>$value,'model'=>'content_atricle'));
                    }
                }
                break;
            case 4:
                //添加到推荐位
                $posId=intval($_POST['position']);
                if(empty($posId)){
                    $this->alert('请选择推荐位！');
                }
                foreach ($_POST['id'] as $value) {
                    $data=array();
                    $data['pos_id']=$posId;
                    $data['content_id']=$value;
                    $data['start_time']=time();
                    $data['stop_time']=time()+31557600*2;
                    $data['status']=1;
                    api('duxcms','addPositionRelation',$data);
                }
                break;
        }
        $this->alert('批量操作成功！');
    }
}