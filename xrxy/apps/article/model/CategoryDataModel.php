<?php
/**
 * CategoryDataModel.php
 * 文章栏目表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class CategoryDataModel extends BaseModel
{
    protected $table = 'category';
    /**
     * 栏目表信息
     * @param int $classId 栏目表ID
     * @return array 栏目信息
     */
    public function getInfo($classId)
    {
        $info = $this->model
                    ->field('A.*,B.catalog_id,B.sub_name,B.name as catalog_name')
                    ->table('category', 'A')
                    ->leftJoin('catalog', 'B', array( 'A.class_id', 'B.class_id' ))
                    ->where('A.site = '.SITEID.' AND A.class_id=' . $classId)
                    ->find();
        if(!empty($info)){
            $appConfig=config('APP');
            $info['curl'] = $this->getUrl($info, $appConfig);
        }
        return $info;
    }
    /**
     * 栏目表信息通过URL
     * @param string $urlName 栏目url
     * @return array 栏目信息
     */
    public function getInfoUrl($urlName)
    {
        return $this->find('site = '.SITEID.' AND urlname="' . $urlName .'"');
    }
    /**
     * 通过ID获取栏目超链接
     * @param string $urlName 栏目url
     * @return array 栏目信息
     */
    public function getCurl($data)
    {
        if(empty($data['classId'])){
            return;
        }
        $info = $this->find('site = '.SITEID.' AND class_id="' . $data['classId'] .'"');
        if(empty($info)){
            return;
        }
        $appConfig=config('APP');
        return $this->getUrl($info,$appConfig);
    }
    /**
     * 获取栏目超链接
     * @param array $data 栏目数据
     * @param array $config APP配置信息
     * @param bool $page 是否分页
     * @param array $append 附加参数
     * @return string 栏目链接
     */
    public function getUrl($data,$config,$page=false)
    {
        if($config['URL_REWRITE_ON']){
            $rewrite = config('REWRITE');
            $rewrite = array_flip($rewrite);
            $url=$rewrite[$data['app'].'/Category/index'];
            $parameter=array('class_id'=>$data['class_id']);
            if(!empty($url)){
                $parameter=array();
                if(strpos($url,'<class_id>')){
                    $parameter['class_id']=$data['class_id'];
                }
                if(strpos($url,'<urlname>')){
                    $parameter['urlname']=$data['urlname'];
                }
            }
        }else{
            $parameter=array('class_id'=>$data['class_id']);
        }
        if($page){
            $parameter['page']='{page}';
        }
        $url = url($data['app'].'/Category/index',$parameter);
        return urldecode($url);
    }
}