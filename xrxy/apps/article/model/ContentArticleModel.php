<?php
/**
 * ContentArticleModel.php
 * 文章表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class ContentArticleModel extends BaseModel
{
    protected $table = 'content_article';
    /**
     * 文章表信息
     * @param int $contentId 文章表ID
     * @return array 用户信息
     */
    public function getInfo($contentId)
    {
        return $this->find('content_id=' . $contentId);
    }
    /**
     * 添加文章表信息
     * @param array $data 文章表信息
     * @return int 文章表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 保存文章表信息
     * @param array $data 文章表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->update('content_id in(' . $data['content_id'].')', $data);
    }
    /**
     * 删除文章表信息
     * @param string $contentId 文章表ID
     * @return bool 状态
     */
    public function delData($contentId)
    {
        //删除关联文章
        return $this->delete('content_id in(' . $contentId .')');
    }
    /**
     * 数据格式化
     * @param array $data 文章信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $data['content_id']=intval($data['content_id']);
        $data['content']=html_in($data['content']);
        return $data;
    }
}