<?php
/**
 * ContentDataModel.php
 * 文章表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class ContentDataModel extends BaseModel
{
    protected $table = 'content';
    /**
     * 文章列表
     * @param string $condition 查询条件
     * @param int $limit 条数
     * @param string $order 主排序
     * @param string $extTable 扩展表
     * @return array 用户信息
     */
    public function loadData($condition,$limit,$order,$extTable=null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $field='A.*,B.name as cname,B.app,B.urlname as urlname';
        $obj=$this->model
                    ->field($field)
                    ->table('content', 'A')
                    ->join('category', 'B', array( 'A.class_id', 'B.class_id' ));
        //扩展字段表
        if(!empty($extTable)){
            $obj=$obj->field('C.*'.','.$field)
                    ->leftJoin('fieldset_'.$extTable, 'C', array( 'C.content_id', 'A.content_id' ));
        }
        //文章列表
        $list=$obj->where($condition)
                    ->order($order.',A.content_id DESC')
                    ->limit($limit)
                    ->select();
        return $list;
    }
    /**
     * 文章总数
     * @param string $condition 查询条件
     * @return int 数量
     */
    public function countData($condition)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model
                    ->table('content', 'A')
                    ->join('category', 'B', array( 'A.class_id', 'B.class_id' ))
                    ->where($condition)
                    ->count();
    }
    /**
     * 查询内容表信息
     * @param string $where 条件
     * @return array 信息
     */
    public function getInfoWhere($condition,$order = ' A.time DESC')
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        $list = $this->model
                    ->field('A.*,B.urlname,B.app,B.name as cname')
                    ->table('content', 'A')
                    ->join('category', 'B', array( 'A.class_id', 'B.class_id' ))
                    ->where($condition)
                    ->order($order)
                    ->limit(1)
                    ->select();
        return $list[0];
    }
    /**
     * 内容表信息
     * @param int $contentId 内容表ID
     * @return array 信息
     */
    public function getInfo($contentId)
    {
        return $this->find('site = '.SITEID.' AND content_id=' . $contentId .' AND status=1');
    }
    /**
     * 根据URL获取内容
     * @param string $urlTitle 内容URL
     * @return array 信息
     */
    public function getInfoUrl($urlTitle)
    {
        return $this->find('site = '.SITEID.' AND urltitle="' . $urlTitle .'"' .' AND status=1');
    }
    /**
     * 更新内容信息
     * @param array $data 内容数组
     * @return bool 状态
     */
    public function saveInfo($data)
    {
        return $this->update('site = '.SITEID.' AND content_id='.$data['content_id'],$data);
    }
    /**
     * 通过ID获取内容超链接
     * @param string $urlName 栏目url
     * @return array 栏目信息
     */
    public function getAurl($data)
    {
        if(empty($data['contentId'])){
            return;
        }
        $info = $this->getInfoWhere('A.content_id='.$data['contentId'].' AND B.app="article"');
        if(empty($info)){
            return;
        }
        $appConfig = config('APP');
        return $this->getUrl($info,$appConfig);
    }
    /**
     * 获取内容超链接
     * @param array $data 内容数据
     * @param array $config APP配置信息
     * @param bool $page 是否分页
     * @return string 内容链接
     */
    public function getUrl($data,$config,$page=false)
    {
        if($config['URL_REWRITE_ON']){
            $rewrite = config('REWRITE');
            $rewrite = array_flip($rewrite);
            $url=$rewrite[$data['app'].'/Info/index'];
            $parameter=array('content_id'=>$data['content_id']);
            if(!empty($url)){
                $parameter=array();
                if(strpos($url,'<content_id>')){
                    $parameter['content_id']=$data['content_id'];
                }
                if(strpos($url,'<class_id>')){
                    $parameter['class_id']=$data['class_id'];
                }
                if(strpos($url,'<urltitle>')){
                    $parameter['urltitle']=$data['urltitle'];
                }
                if(strpos($url,'<dir>')){
                    $parameter['dir']=$data['urlname'];
                }
                if(strpos($url,'<date>')){
                    $parameter['yy']=date('Y-m-d',$data['time']);
                }
            }
        }else{
            $parameter=array('content_id'=>$data['content_id']);
        }
        if($page){
            $parameter['page']='{page}';
        }
        $url = url($data['app'].'/Info/index',$parameter);
        return $url;
    }
    /**
     * 文章列表标签
     * @param array $data 标签信息
     * @return array 菜单列表
     */
    public function loadLabelList($data)
    {
        $where=array();
        if(!empty($data['classId'])){
            $where['class_id']=' AND B.class_id in(' . $data['classId'] . ')';
        }
        if ($data['type']=='sub'&&!empty($data['classId'])) {
            $catalogInfo=api('duxcms','getCatalogClass',array('class_id'=>$data['classId']));
            $classIds = api('duxcms','getCatalogSubClass',array('catalog_id'=>$catalogInfo['catalog_id']));
            $where['class_id'] = " AND B.class_id in (" . $classIds .")";
        }
        if (isset($data['image'])) {
            if($data['image'] == true)
            {
                $where['image'] = ' AND A.image<>"" ';
            }else{
                $where['image'] = ' AND A.image = "" ';
            }
        }
        if (!empty($data['where'])) {
            $where['where'] = ' AND '.$data['where'];
        }
        if (empty($data['limit'])) {
            $data['limit'] = 10;
        }
        if(empty($data['order'])){
            $data['order']='A.time DESC';
        }
        if($data['top'] == true)
        {
            $order = 'A.type_top > '.time().' DESC';
            $data['order'] = $order.','.$data['order'];
        }
        $where=implode(' ', $where);
        $list=$this->loadData('A.status=1'.$where,$data['limit'],$data['order'],$data['extTable']);
        if(empty($list)){
            return array();
        }
        $appConfig=config('APP');
        $data=array();
        foreach ($list as $key => $value) {
            $data[$key]=$value;
            $data[$key]['curl']=model('CategoryData')->getUrl($value, $appConfig);
            $data[$key]['aurl']=$this->getUrl($value, $appConfig);
        }
        return $data;
        
    }
}