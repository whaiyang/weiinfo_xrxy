<?php
/**
 * articleApi.php
 * 文章系统API
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class articleApi extends BaseApi
{
	/**
     *菜单Api
     */
    public function apiAdminMenu()
    {
        return array(
            'content' => array(
                'menu' => array(
                    'content' => array(
                    	'name' => '内容管理',
                        'menu' => array(
                            array(
                                'name' => '文章栏目',
                                'url' => url('AdminCategory/index'),
                                'ico' => '&#xf07c;',
                                'order' => 0
                            ),
                            array(
                                'name' => '文章管理',
                                'url' => url('AdminContent/index'),
                                'ico' => '&#xf0cb;',
                                'order' => 1
                            )
                        ),
                    ),
                )
            )
        );
    }
    /**
     *模型Api
     */
    public function apiContentModel()
    {
        return array(
            'name'=>'文章',
            'listType'=>1,
            'order'=>0,
            );
    }
    /**
     *文章模板列表标签
     */
    public function apiLabelArticleList($data){
        return model('ContentData')->loadLabelList($data);
    }
    /**
     *文章栏目列表标签
     */
    public function apiLabelArticleCategory($data){
        return model('CategoryArticle')->loadLabelList($data);
    }
    /**
     *文章栏目链接标签
     */
    public function apiLabelArticleCurl($data){
        return model('CategoryData')->getCurl($data);
    }
    /**
     *文章链接标签
     */
    public function apiLabelArticleAurl($data){
        return model('ContentData')->getAurl($data);
    }
    /**
     *文章栏目URL
     */
    public function getCurl($data){
        return model('CategoryData')->getUrl($data['data'], $data['config']);
    }
    /**
     *文章URL
     */
    public function getAurl($data){
        return model('ContentData')->getUrl($data['data'], $data['config']);
    }

}