<?php
/**
 * AdminPagesController.php
 * 内容模型操作
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminPagesController extends AdminController
{
    /**
     * 主页面
     */
    public function index()
    {
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
        );
        $url = url('AdminPages/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //页面列表信息
        $list = api('duxcms', 'getCategoryTree',array('where'=>'app="'.APP_NAME.'"'));
        $count = api('duxcms', 'countCategoryData',array('where'=>'app="'.APP_NAME.'"'));
        $data=array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key]=$value;
                $data[$key]['curl']=model('CategoryData')->getUrl($value, $appConfig);
            }
        }
        $list = $data;
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加页面
     */
    public function add()
    {
        $appConfig = appConfig('pages');
        $info = $appConfig['APP_MODEL_CONFIG'];
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('categoryList', api('duxcms','getCategoryTree',array('where'=>'app="'.APP_NAME.'"')));
        $this->assign('catalogList', api('duxcms','getCatalogTree'));
        $this->show('AdminPages/info');
    }
    /**
     * 处理页面添加
     */
    public function addData()
    {
        $_POST['app']=APP_NAME;
        $list = explode("\n", $_POST['namelist']);
        if(empty($list)){
            $this->msg('页面名称未填写',false);
        }
        if(empty($_POST['class_tpl'])){
            $this->msg('页面或内容模板未设置！',false);
        }
        $_POST['type']=0;
        $_POST['class_id']=api('duxcms', 'addCategoryData',$_POST);
        model('CategoryPage')->addData($_POST);
        //添加到菜单
        if($_POST['catalog_id']<>'no'){
            $menuData=array();
            $menuData['parent_id']=$_POST['catalog_id'];
            $menuData['type']=1;
            $menuData['class_id']=$_POST['class_id'];
            $menuData['status']=1;
            api('duxcms', 'addCatalogData',$menuData);
        }
        //关联附件
        api('admin','relationFile',array('relation_id'=>$_POST['class_id'],'model'=>'category_pages','relation_key'=>$_POST['relation_key']));
        $this->msg('页面添加成功！');
    }
    /**
     * 编辑页面
     */
    public function edit()
    {
        $classId = intval($_GET['class_id']);
        if (empty($classId)) {
            $this->msg('无法获取页面ID！', false);
        }
        //页面信息
        $info = api('duxcms', 'getCategoryData',array('class_id'=>$classId));
        $infoPage=model('CategoryPage')->getInfo($classId);
        $info['content']=$infoPage['content'];
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('categoryList', api('duxcms','getCategoryTree',array('where'=>'app="'.APP_NAME.'"')));
        $this->show('AdminPages/info');
    }
    /**
     * 处理页面
     */
    public function editData()
    {
        $data=in($_POST);
        if (empty($data['class_id'])) {
            $this->msg('无法获取页面ID！', false);
        }
        //检查
        if(empty($data['name'])){
            $this->msg('页面名称未填写',false);
        }
        if(empty($data['class_tpl'])&&empty($data['content_tpl'])){
            $this->msg('页面或内容模板未选择！',false);
        }
        // 分类检测
        if ($data['parent_id'] == $data['class_id']){
            $this->msg('不可以将当前页面设置为上一级页面！',false);
            return;
        }
        $cat = api('duxcms','getCategoryTree',array('class_id'=>$data['class_id']));
        if (!empty($cat)) {
            foreach ($cat as $vo) {
                if ($data['parent_id'] == $vo['class_id']) {
                    $this->msg('不可以将上一级页面移动到子页面！',false);
                    return;
                }
            }
        }
        api('duxcms', 'saveCategoryData',$_POST);
        model('CategoryPage')->saveData($_POST);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$data['class_id'],'model'=>'category_pages','relation_key'=>$_POST['relation_key']));
        $this->msg('页面修改成功！');
    }
    /**
     * 删除页面
     */
    public function del()
    {
        $classId=intval($_GET['class_id']);
        if (empty($classId)) {
            $this->msg('无法获取页面ID！', false);
        }
        if(api('duxcms', 'countCategoryData',array('where'=>'parent_id='.$classId))){
            $this->msg('请先删除子页面！',false);
        }
        //删除页面
        api('duxcms', 'delCategoryData',array('class_id'=>$classId));
        model('CategoryPage')->delData($classId);
        //关联附件
        api('admin','delRelationFile',array('relation_id'=>$classId,'model'=>'category_pages'));
        $this->msg('页面删除成功！',1);
    }
}