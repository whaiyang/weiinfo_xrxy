<?php
/**
 * CategoryPageModel.php
 * 页面表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class CategoryPageModel extends BaseModel
{
    protected $table = 'category_page';
    /**
     * 页面表信息
     * @param int $classId 页面表ID
     * @return array 用户信息
     */
    public function getInfo($classId)
    {
        return $this->find('class_id=' . $classId);
    }
    /**
     * 添加页面表信息
     * @param array $data 页面表信息
     * @return int 页面表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->insert($data);
    }
    /**
     * 保存页面表信息
     * @param array $data 页面表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        return $this->update('class_id=' . $data['class_id'], $data);
    }
    /**
     * 删除页面表信息
     * @param int $classId 页面表ID
     * @return bool 状态
     */
    public function delData($classId)
    {
        //删除关联内容
        return $this->delete('class_id=' . $classId);
    }
    /**
     * 数据格式化
     * @param array $data 页面信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $data['class_id']=intval($data['class_id']);
        $data['content']=html_in($data['content']);
        return $data;
    }
}