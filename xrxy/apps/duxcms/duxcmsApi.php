<?php
/**
 * duxcmsApi.php
 * cms基础API
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class duxcmsApi extends BaseApi
{
    /**
     *菜单Api
     */
    public function apiAdminMenu()
    {
        return array(
            'content' => array(
                'name' => '内容',
                'order' => 1,
                'menu' => array(
                    'category' => array(
                        'name' => '分类管理',
                        'order' => 0,
                        'menu' => array(
                            array(
                                'name' => '菜单管理',
                                'url' => url('AdminCatalog/index'),
                                'ico' => '&#xf02d;',
                                'order' => 0
                            )
                        )
                    ),
                    'function' => array(
                        'name' => '内容功能',
                        'order' => 10,
                        'menu' => array(
                            array(
                                'name' => 'TAG管理',
                                'url' => url('AdminTags/index'),
                                'ico' => '&#xf02c;',
                                'order' =>0
                            ),
                            array(
                                'name' => '替换管理',
                                'url' => url('AdminReplace/index'),
                                'ico' => '&#xf0ec;',
                                'order' => 1
                            ),
                            array(
                                'name' => '预设管理',
                                'url' => url('AdminModel/index'),
                                'ico' => '&#xf076;',
                                'order' =>2
                            ),
                        ),
                    ),
                    'position' => array(
                        'name' => '推荐位',
                        'order' => 9,
                        'menu' => array(
                            array(
                                'name' => '推荐位管理',
                                'url' => url('AdminPosition/index'),
                                'ico' => '&#xf0fe;',
                                'order' =>0
                            ),
                            array(
                                'name' => '推荐内容',
                                'url' => url('AdminPositionRelation/index'),
                                'ico' => '&#xf0fd;',
                                'order' => 1
                            )
                        ),
                    ),
                )
            ),
            'expand' => array(
                'name' => '功能',
                'order' => 2,
                'menu' => array(
                    'system' => array(
                        'name' => '系统功能',
                        'order' => 0,
                        'menu' => array(
                            array(
                                'name' => '字段集管理',
                                'url' => url('AdminFieldset/index'),
                                'ico' => '&#xf029;',
                                'order' => 0
                            ),
                            array(
                                'name' => '表单管理',
                                'url' => url('AdminForm/index'),
                                'ico' => '&#xf0db;',
                                'order' => 1
                            ),
                            array(
                                'name' => '碎片管理',
                                'url' => url('AdminFragment/index'),
                                'ico' => '&#xf043;',
                                'order' => 2
                            )
                        )
                    ),
                    'form' => array(
                        'name' => '用户表单',
                        'order' => 1,
                        'menu' => model('Form')->loadFormMenu(),
                    ),
                )
            ),
            'system' => array(
                'menu' => array(
                    'setting' => array(
                        'menu' => array(
                            array(
                                'name' => '站点设置',
                                'url' => url('AdminSiteConfig/index'),
                                'ico' => '&#xf085;',
                                'order' => 2
                            ),
                        )
                    ),
                    'manage' => array(
                        'menu' => array(
                            array(
                                'name' => '站点管理',
                                'url' => url('AdminSiteManage/index'),
                                'ico' => '&#xf0e8;',
                                'order' => 1
                            ),
                             array(
                                'name' => 'URL规则',
                                'url' => url('AdminRewrite/index'),
                                'ico' => '&#xf0d0;',
                                'order' => 2
                            ),
                        )
                    ),
                )
            )
        );
    }

    /**
     *设置站点配置信息
     * @return array 数组
     */
    public function setSiteConfig()
    {
        return model('SiteConfig')->setSite();
    }

    /**
     *获取站点信息
     * @return array 数组
     */
    public function getSiteInfo($siteId)
    {
        return model('Site')->getInfo($siteId);
    }

    /**
     *获取站点列表
     * @return array 数组
     */
    public function getSiteList()
    {
        return model('Site')->loadData();
    }

    /**
     *获取模型名称
     * @return array 名称数组
     */
    public function getModelName()
    {
        return model('ModelData')->getName();
    }
    /**
     *获取菜单树
     * @param string $data['where'] 条件
     * @param int $data['class_id'] 父级ID
     * @return array 栏目表列表
     */
    public function getCatalogTree($data)
    {
        return model('Catalog')->getTrueData($data['where'],$data['catalog_id']);
    }
    /**
     *根据栏目ID获取菜单
     * @param int $data['class_id'] 栏目ID
     * @return array 菜单信息
     */
    public function getCatalogClass($data)
    {
        return model('Catalog')->getInfoClass($data['class_id']);
    }
    /**
     *获取子菜单栏目ID
     * @param int $data['class_id'] 栏目ID
     * @return array 菜单信息
     */
    public function getCatalogSubClass($data)
    {
        return model('Catalog')->getSubClass($data['catalog_id']);
    }
    /**
     *获取栏目分类树
     * @param string $data['where'] 条件
     * @param int $data['class_id'] 父级ID
     * @return array 栏目表列表
     */
    public function getCategoryTree($data)
    {
        return model('Category')->getTrueData($data['where'],$data['class_id']);
    }
    /**
     *获取栏目信息
     * @param int $data['class_id'] 栏目ID
     * @return array 栏目信息
     */
    public function getCategoryData($data)
    {
        return model('Category')->getInfo($data['class_id']);
    }
    /**
     * 通过URL获取栏目信息
     * @param int $data['urlname'] 栏目ID
     * @return array 栏目信息
     */
    public function getCategoryUrlData($data)
    {
        return model('Category')->getInfoUrl($data['urlname']);
    }
    /**
     * 获取栏目URL
     * @param int $data['urlname'] 栏目ID
     * @return array 栏目信息
     */
    public function getCategoryUrl($data)
    {
        return model('Category')->getCurl($data['data'],$data['config'],$data['page'],$data['append']);
    }
    /**
     * 获取菜单面包屑
     * @param int $data['catalog_id'] 菜单ID
     * @return array 菜单数组
     */
    public function loadCatalogCrumb($data)
    {
        return model('Catalog')->loadCrumb($data['catalog_id']);
    }
    /**
     *获取栏目统计
     * @param string $data['where'] 条件
     * @return array 栏目数量
     */
    public function countCategoryData($data)
    {
        return model('Category')->countData($data['where']);
    }
    /**
     *添加栏目信息
     * @param array $data 栏目数据
     * @return int 栏目ID
     */
    public function addCategoryData($data)
    {
        return model('Category')->addData($data);
    }
    /**
     *保存栏目信息
     * @param array $data 栏目数据
     * @return bool 状态
     */
    public function saveCategoryData($data)
    {
        return model('Category')->saveData($data);
    }
    /**
     *删除栏目信息
     * @param int $data['class_id'] 栏目ID
     * @return bool 状态
     */
    public function delCategoryData($data)
    {
        return model('Category')->delData($data['class_id']);
    }
    /**
     *获取内容列表
     * @param string $data['where'] 条件
     * @return array 内容表列表
     */
    public function loadContentData($data)
    {
        return model('Content')->loadData($data['where'],$data['limit']);
    }
    /**
     *获取内容信息
     * @param int $data['content_id'] 内容ID
     * @return array 内容信息
     */
    public function getContentData($data)
    {
        return model('Content')->getInfo($data['content_id']);
    }
    /**
     *获取内容统计
     * @param string $data['where'] 条件
     * @return array 内容数量
     */
    public function countContentData($data)
    {
        return model('Content')->countData($data['where']);
    }
    /**
     *添加内容信息
     * @param array $data 内容数据
     * @return int 内容ID
     */
    public function addContentData($data)
    {
        return model('Content')->addData($data);
    }
    /**
     *保存内容信息
     * @param array $data 内容数据
     * @return bool 状态
     */
    public function saveContentData($data)
    {
        return model('Content')->saveData($data);
    }
    /**
     *删除内容信息
     * @param string $data['content_id'] 内容ID
     * @return bool 状态
     */
    public function delContentData($data)
    {
        return model('Content')->delData($data['content_id']);
    }

    /**
     *获取字段集
     * @return array 字段集列表
     */
    public function loadFieldsetData()
    {
        return model('Fieldset')->loadData();
    }
    /**
     *检测字段录入
     * @param array $data 录入列表
     * @return array 状态数组
     */
    public function checkFieldsetData($data)
    {
        return model('Fieldset')->checkField($data);
    }
    /**
     * 获取模板标签返回
     * @param array $data 标签数组
     * @return array 查询数组
     */
    public function getTplLabel($data)
    {
        return model('TplLabel')->getLabel($data);
    }
    /**
     * 模板菜单列表标签
     * @param array $data 标签数组
     * @return array 菜单列表
     */
    public function apiLabelMenuList($data)
    {
        return model('Catalog')->loadLabelList($data);
    }
    /**
     * 获取前台MEDIA信息
     * @param array $data MEDIA信息
     * @return array 信息数组
     */

    public function getReceptionMedia($data)
    {
        return model('Reception')->getMedia($data['config'],$data['title'],$data['keywords'],$data['description']);
    }
    /**
     * 处理数据替换
     * @param string $data['data'] 数据内容
     * @return string 处理后数据
     */
    public function getReplaceContent($data)
    {
        return model('Replace')->replaceData($data['data']);
    }
    /**
     * 替换tag链接
     * @param array $data['data'] 数据内容
     * @param int $data['content_id'] 内容ID
     * @return string 处理后数据
     */
    public function getTagsContent($data)
    {
        return model('Tags')->replaceData($data['data'],$data['content_id']);
    }
    /**
     *TAG列表标签
     */
    public function apiLabelTagList($data){
        return model('Tags')->loadLabelList($data);
    }
    /**
     *TAG内容列表标签
     */
    public function apiLabelTagContent($data){
        return model('TagsRelation')->loadLabelList($data);
    }
    /**
     *表单列表标签
     */
    public function apiLabelFormList($data){
        return model('FormData')->loadLabelList($data);
    }
    /**
     *表单HTML标签
     */
    public function apiLabelFormHtml($data){
        return model('Form')->getFormHtml($data);
    }
    /**
     *表单链接标签
     */
    public function apiLabelFormUrl($data){
        return model('Form')->getFormUrl($data);
    }
    /**
     *碎片调用标签
     */
    public function apiLabelFragment($data){
        return model('Fragment')->getLabel($data);
    }
    /**
     *添加到推荐位
     * @param array $data 推荐数据
     * @return int 推荐ID
     */
    public function addPositionRelation($data)
    {
        return model('PositionRelation')->addDataContent($data);
    }
    /**
     *删除推荐位内容
     * @param array $data 推荐数据
     * @return int 推荐ID
     */
    public function delPositionRelation($data)
    {
        return model('PositionRelation')->delData($data['data_id']);
    }
    /**
     *获取推荐位列表
     * @param array $data 推荐数据
     * @return int 推荐ID
     */
    public function loadPositionList()
    {
        return model('Position')->loadData();
    }
    /**
     *推荐内容列表标签
     */
    public function apiLabelPositionList($data){
        return model('PositionRelation')->loadLabelList($data);
    }
    /**
     * 获取字段处理类型
     */
    public function getFieldType()
    {
        return model('Field')->typeField();
    }
    /**
     * 获取字段处理属性
     */
    public function getFieldProperty()
    {
        return model('Field')->propertyField();
    }
    /**
     * 格式化字段
     */
    public function getFieldForamt($data)
    {
        return model('Field')->formatField($data['data'],$data['type']);
    }
    

}