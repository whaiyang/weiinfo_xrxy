<?php
/**
 * AdminFragmentController.php
 * 碎片管理
 * @author Life <349865361@qq.com>
 * @version 20140125
 */
class AdminFragmentController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' key LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminFragment/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //碎片列表信息
        $list = model('Fragment')->loadData($where, $limit);
        $count = model('Fragment')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加碎片
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminfragment/info');
    }
    /**
     * 处理碎片添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('碎片名称不能为空！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('碎片内容不能为空！', false);
        }
        $_POST['content'] = html_in($_POST['content']);
        model('Fragment')->addData($_POST);
        $this->msg('碎片添加成功！', 1);
    }
    /**
     * 编辑碎片资料
     */
    public function edit()
    {
        $replaceId = intval($_GET['fragment_id']);
        if (empty($replaceId)) {
            $this->msg('无法获取碎片ID！', false);
        }
        //碎片信息
        $info = model('Fragment')->getInfo($replaceId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminfragment/info');
    }
    /**
     * 处理碎片资料
     */
    public function editData()
    {
        $replaceId = intval($_POST['fragment_id']);
        if (empty($replaceId)) {
            $this->msg('无法获取碎片ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('碎片名称不能为空！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('碎片内容不能为空！', false);
        }
        $_POST['content'] = html_in($_POST['content']);
        model('Fragment')->saveData($_POST);
        $this->msg('碎片修改成功！', 1);
    }
    /**
     * 删除碎片
     */
    public function del()
    {
        $replaceId = intval($_POST['data']);
        if (empty($replaceId)) {
            $this->msg('碎片ID无法获取！', false);
        }
        model('Fragment')->delData($replaceId);
        $this->msg('碎片删除成功！');
    }
}