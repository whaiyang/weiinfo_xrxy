<?php
/**
 * AdminFieldController.php
 * 内容字段管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminFieldController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        $fieldsetId = intval($_GET['fieldset_id']);
        if (empty($fieldsetId)) {
            $this->msg('无法获取字段ID！', false);
        }
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'fieldset_id' => $fieldsetId,
            'keyword' => $filterKeyword,
        );
        $url = url('AdminField/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'fieldset_id='.$fieldsetId.$filterWhere;
        //字段列表信息
        $list = model('Field')->loadData($where, $limit);
        $count = model('Field')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('fieldsetInfo', model('Fieldset')->getInfo($fieldsetId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加字段
     */
    public function add()
    {
        $fieldsetId = intval($_GET['fieldset_id']);
        if (empty($fieldsetId)) {
            $this->msg('无法获取字段集ID！', false);
        }

        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('fieldsetInfo', model('Fieldset')->getInfo($fieldsetId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('propertyField', model('Field')->propertyField());
        $this->show('adminfield/info');
    }
    /**
     * 处理字段添加
     */
    public function addData()
    {
        if (empty($_POST['fieldset_id'])) {
            $this->msg('无法获取字段集ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('Field')->getInfoRepeat($_POST['field'],$_POST['fieldset_id'])){
            $this->msg('字段不能重复！', false);
        }
        model('Field')->addData($_POST);
        $this->msg('字段添加成功！', 1);
    }
    /**
     * 编辑字段资料
     */
    public function edit()
    {
        $fieldsetId = intval($_GET['fieldset_id']);
        if (empty($fieldsetId)) {
            $this->msg('无法获取字段ID！', false);
        }
        $fieldId = intval($_GET['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        //字段信息
        $info = model('Field')->getInfo($fieldId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('fieldsetInfo', model('Fieldset')->getInfo($fieldsetId));
        $this->assign('typeField', model('Field')->typeField());
        $this->assign('propertyField', model('Field')->propertyField());
        $this->show('adminfield/info');
    }
    /**
     * 处理字段资料
     */
    public function editData()
    {
        $fieldId = intval($_POST['field_id']);
        if (empty($fieldId)) {
            $this->msg('无法获取字段ID！', false);
        }
        if (empty($_POST['fieldset_id'])) {
            $this->msg('字段无法获取！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['field'])) {
            $this->msg('字段名不能为空！', false);
        }
        if (empty($_POST['type'])) {
            $this->msg('字段类型未选择！', false);
        }
        if(model('Field')->getInfoRepeat($_POST['field'],$_POST['fieldset_id'],$fieldId)){
            $this->msg('字段不能重复！', false);
        }
        model('Field')->saveData($_POST);
        $this->msg('字段修改成功！', 1);
    }
    /**
     * 删除字段
     */
    public function del()
    {
        $fieldId = intval($_POST['data']);
        if (empty($fieldId)) {
            $this->msg('字段ID无法获取！', false);
        }
        model('Field')->delData($fieldId);
        $this->msg('字段删除成功！');
    }
}