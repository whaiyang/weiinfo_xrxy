<?php
/**
 * AdminPositionRelationController.php
 * 推荐内容管理
 * @author Life <349865361@qq.com>
 * @version 20140125
 */
class AdminPositionRelationController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterPosition = intval($_GET['pos_id']);
        $filterClass = intval($_GET['class_id']);
        $filterWhere = '';
        if (!empty($filterPosition)) {
            $filterWhere .= ' AND A.pos_id = '.$filterPosition;
        }
        if (!empty($filterClass)) {
            $filterWhere .= ' AND C.class_id = '.$filterClass;
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'pos_id' => $filterPosition,
            'class_id' => $filterClass,
        );
        $url = url('AdminPositionRelation/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = substr($filterWhere, 4);
        //推荐内容列表信息
        $list = model('PositionRelation')->loadData($where, $limit);
        $count = model('PositionRelation')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->assign('categoryList', api('duxcms','getCategoryTree'));
        $this->assign('positionList', model('Position')->loadData());
        $this->show();
    }
    /**
     * 添加推荐内容
     */
    public function add()
    {
        $positionList = model('Position')->loadData();
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('positionList', $positionList);
        $this->show('adminpositionrelation/info');
    }
    /**
     * 处理推荐内容添加
     */
    public function addData()
    {
        $posId = intval($_POST['pos_id']);
        if (empty($posId)) {
            $this->msg('无法获取推荐位ID！', false);
        }
        $_POST['start_time']=time();
        $_POST['start_time']=time()+31557600*2;
        model('PositionRelation')->addData($_POST);
        $this->msg('推荐内容添加成功！', 1);
    }
    /**
     * 编辑推荐内容资料
     */
    public function edit()
    {
        $dataId = intval($_GET['data_id']);
        if (empty($dataId)) {
            $this->msg('无法获取推荐内容ID！', false);
        }
        //推荐内容信息
        $info = model('PositionRelation')->getInfo($dataId);
        $contentInfo = model('Content')->getInfo($info['content_id']);
        $positionList = model('Position')->loadData();
        //模板赋值
        $this->assign('info', $info);
        $this->assign('contentInfo', $contentInfo);
        $this->assign('positionList', $positionList);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminpositionrelation/info');
    }
    /**
     * 处理推荐内容资料
     */
    public function editData()
    {
        $dataId = intval($_POST['data_id']);
        if (empty($dataId)) {
            $this->msg('无法获取推荐内容ID！', false);
        }
        $posId = intval($_POST['pos_id']);
        if (empty($posId)) {
            $this->msg('无法获取推荐位ID！', false);
        }
        model('PositionRelation')->saveData($_POST);
        $this->msg('推荐内容修改成功！', 1);
    }
    /**
     * 删除推荐内容
     */
    public function del()
    {
        $posId = intval($_POST['data']);
        if (empty($posId)) {
            $this->msg('推荐内容ID无法获取！', false);
        }
        model('PositionRelation')->delData($posId);
        $this->msg('推荐内容删除成功！');
    }
}