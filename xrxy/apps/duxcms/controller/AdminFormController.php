<?php
/**
 * AdminFormController.php
 * 自定义表单管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminFormController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminForm/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //表单列表信息
        $list = model('Form')->loadData($where, $limit);
        $count = model('Form')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加表单
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminform/info');
    }
    /**
     * 处理表单添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['table'])) {
            $this->msg('表单表不能为空！', false);
        }
        if(model('Form')->getInfoRepeat($_POST['table'])){
            $this->msg('表单表不能重复！', false);
        }
        model('Form')->addData($_POST);
        api('admin','resetCache',array('type'=>'menu'));
        $this->msg('表单添加成功！', 1);
    }
    /**
     * 编辑表单资料
     */
    public function edit()
    {
        $formId = intval($_GET['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        //表单信息
        $info = model('Form')->getInfo($formId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminform/info');
    }
    /**
     * 处理表单资料
     */
    public function editData()
    {
        $formId = intval($_POST['form_id']);
        if (empty($formId)) {
            $this->msg('无法获取表单ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('名称未填写！', false);
        }
        if (empty($_POST['table'])) {
            $this->msg('表单表不能为空！', false);
        }
        if(model('Form')->getInfoRepeat($_POST['table'],$formId)){
            $this->msg('表单表不能重复！', false);
        }
        model('Form')->saveData($_POST);
        api('admin','resetCache',array('type'=>'menu'));
        $this->msg('表单修改成功！', 1);
    }
    /**
     * 删除表单
     */
    public function del()
    {
        $formId = intval($_POST['data']);
        if (empty($formId)) {
            $this->msg('表单ID无法获取！', false);
        }
        model('Form')->delData($formId);
        model('FormField')->delDataForm($formId);
        api('admin','resetCache',array('type'=>'menu'));
        $this->msg('表单删除成功！');
    }
    /**
     * AJAX获取表单列表
     */
    public function getField()
    {
        $classId=$_POST['class_id'];
        if(empty($classId)){
            return;
        }
        //获取栏目信息
        $classInfo=model('Category')->getInfo($classId);
        if(empty($classInfo)||!$classInfo['form_id']){
            return;
        }
        //获取表单列表
        $formInfo=model('Form')->getInfo($classInfo['form_id']);
        if(empty($formInfo)){
            return;
        }
        $fieldList=model('Field')->loadData('form_id='.$classInfo['form_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        $typeField=model('Field')->typeField();
        //获取扩展内容信息
        $contentId=$_POST['content_id'];
        if(!empty($contentId)){
            $contentInfo=model('FieldData')->getInfoContent($formInfo['table'],$contentId);
        }
        $html='';
        $form=new DuxForm();
        foreach ($fieldList as $value) {
            $method=$typeField[$value['type']]['html'];
            $config=array();
            $config['title']=$value['name'];
            $config['name']='Form_'.$value['field'];
            if($contentInfo[$value['field']]){
                $config['value']=$contentInfo[$value['field']];
            }else{
                $config['value']=$value['default'];
            }
            $config['size']=$value['size'];
            $config['datatype']=unserialize($value['datatype']);
            $config['tip']=$value['tip'];
            $config['errormsg']=$value['errormsg'];
            $list=explode(',', $value['config']);
            $newList=array();
            if(!empty($list)){
                foreach ($list as $key => $value) {
                    $newList[$key]['value']=$key+1;
                    $newList[$key]['title']=$value;
                }
            }
            $config['list']=$newList;
            $html.=$form->$method($config);
        }
        echo $html;
    }
}