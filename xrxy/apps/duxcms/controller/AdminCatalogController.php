<?php
/**
 * AdminCatalogController.php
 * 内容分类
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminCatalogController extends AdminController
{
    /**
     * 主页面
     */
    public function index()
    {
        //模型列表
        $modelList=model('ModelData')->loadData();
        //菜单列表信息
        $list = model('Catalog')->getTrueData($where);
        $count = count($list);
        //模板赋值
        $this->assign('modelList', $modelList);
        $this->assign('modelNameArray', model('ModelData')->getName());
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->show();
    }
     /**
     * 添加菜单
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('catalogList', model('Catalog')->getTrueData());
        $this->show('AdminCatalog/info');
    }
     /**
     * 处理菜单添加
     */
    public function addData()
    {
        $data=in($_POST);
        if(empty($data['url'])||empty($data['name'])){
            $this->msg('绑定链接或者菜单名未填写！',false);
        }
        if(!empty($data['parent_id'])){
            $parentInfo = model('Catalog')->getInfo($data['parent_id']);
            if(empty($parentInfo['class_id'])){
                $this->msg('上级菜单不能为链接型菜单！',false);
            }
        }
        model('Catalog')->addData($data);
        $this->msg('菜单添加成功！');
    }
    /**
     * 编辑菜单
     */
    public function edit()
    {
        $catalogId = intval($_GET['catalog_id']);
        if (empty($catalogId)) {
            $this->msg('无法获取菜单ID！', false);
        }
        //菜单信息
        $info = model('Catalog')->getInfo($catalogId);
        //模板赋值
        if(!empty($info['class_id'])){
            $this->assign('categoryInfo', model('Category')->getInfo($info['class_id']));
        }
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('catalogList', model('Catalog')->getTrueData());
        $this->show('AdminCatalog/info');
    }
    /**
     * 处理菜单
     */
    public function editData()
    {
        $data=in($_POST);
        if (empty($data['catalog_id'])) {
            $this->msg('无法获取菜单ID！', false);
        }
        $info = model('Catalog')->getInfo($data['catalog_id']);
        if(empty($info['class_id'])){
            if(empty($data['url'])||empty($data['name'])){
                $this->msg('绑定链接或者菜单名未填写！',false);
            }
        }
        // 分类检测
        if(!empty($data['parent_id'])){
            $parentInfo = model('Catalog')->getInfo($data['parent_id']);
            if(empty($parentInfo['class_id'])){
                $this->msg('上级菜单不能为链接型菜单！',false);
            }
        }
        if ($data['parent_id'] == $data['catalog_id']){
            $this->msg('不可以将当前菜单设置为上一级菜单！',false);
        }
        $cat = model('Catalog')->getTrueData('',$data['catalog_id']);
        if (!empty($cat)) {
            foreach ($cat as $vo) {
                if ($data['parent_id'] == $vo['catalog_id']) {
                    $this->msg('不可以将上一级菜单移动到子菜单！',false);
                }
            }
        }
        model('Catalog')->saveData($data);
        $this->msg('菜单修改成功！');
    }
    /**
     * 删除菜单
     */
    public function del()
    {
        $catalogId=intval($_GET['catalog_id']);
        if (empty($catalogId)) {
            $this->msg('无法获取菜单ID！', false);
        }
        model('Catalog')->delData($catalogId);
        $this->msg('菜单删除成功！',1);
    }
    /**
     * 排序参数
     */
    public function orderEdit()
    {
        $name=in($_POST['name']);
        $value=intval($_POST['data']);
        $catalog_id=intval($_GET['catalog_id']);
        if(empty($catalog_id)){
            $this->msg('无法定位编辑数据！',false);
        }
        if(empty($name)){
            $this->msg('编辑数据无法获取！',false);
        }
        $data=array();
        $data[$name]=$value;
        $data['catalog_id']=$catalog_id;
        model('Catalog')->saveData($data);
        $this->msg($value);
    }
}