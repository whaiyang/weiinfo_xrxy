<?php
/**
 * AdminReplaceController.php
 * 内容替换管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AdminReplaceController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' key LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminReplace/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //替换列表信息
        $list = model('Replace')->loadData($where, $limit);
        $count = model('Replace')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加替换
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminreplace/info');
    }
    /**
     * 处理替换添加
     */
    public function addData()
    {
        if (empty($_POST['key'])) {
            $this->msg('关键词未填写！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('替换内容不能为空！', false);
        }
        $_POST['num'] = intval($_POST['num']);
        model('Replace')->addData($_POST);
        $this->msg('替换添加成功！', 1);
    }
    /**
     * 编辑替换资料
     */
    public function edit()
    {
        $replaceId = intval($_GET['replace_id']);
        if (empty($replaceId)) {
            $this->msg('无法获取替换ID！', false);
        }
        //替换信息
        $info = model('Replace')->getInfo($replaceId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminreplace/info');
    }
    /**
     * 处理替换资料
     */
    public function editData()
    {
        $replaceId = intval($_POST['replace_id']);
        if (empty($replaceId)) {
            $this->msg('无法获取替换ID！', false);
        }
        if (empty($_POST['key'])) {
            $this->msg('关键词未填写！', false);
        }
        if (empty($_POST['content'])) {
            $this->msg('替换内容不能为空！', false);
        }
        $_POST['num'] = intval($_POST['num']);
        model('Replace')->saveData($_POST);
        $this->msg('替换修改成功！', 1);
    }
    /**
     * 删除替换
     */
    public function del()
    {
        $replaceId = intval($_POST['data']);
        if (empty($replaceId)) {
            $this->msg('替换ID无法获取！', false);
        }
        model('Replace')->delData($replaceId);
        $this->msg('替换删除成功！');
    }
}