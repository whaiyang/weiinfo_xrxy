<?php
/**
 * AdminPositionController.php
 * 推荐位管理
 * @author Life <349865361@qq.com>
 * @version 20140125
 */
class AdminPositionController extends AdminController
{
    /**
     * 列表页
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}',
            'keyword' => $filterKeyword
        );
        $url = url('AdminPosition/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = $filterWhere;
        //推荐位列表信息
        $list = model('Position')->loadData($where, $limit);
        $count = model('Position')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加推荐位
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->show('adminposition/info');
    }
    /**
     * 处理推荐位添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('推荐位名称不能为空！', false);
        }
        model('Position')->addData($_POST);
        $this->msg('推荐位添加成功！', 1);
    }
    /**
     * 编辑推荐位资料
     */
    public function edit()
    {
        $posId = intval($_GET['pos_id']);
        if (empty($posId)) {
            $this->msg('无法获取推荐位ID！', false);
        }
        //推荐位信息
        $info = model('Position')->getInfo($posId);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->show('adminposition/info');
    }
    /**
     * 处理推荐位资料
     */
    public function editData()
    {
        $posId = intval($_POST['pos_id']);
        if (empty($posId)) {
            $this->msg('无法获取推荐位ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('推荐位名称不能为空！', false);
        }
        model('Position')->saveData($_POST);
        $this->msg('推荐位修改成功！', 1);
    }
    /**
     * 删除推荐位
     */
    public function del()
    {
        $posId = intval($_POST['data']);
        if (empty($posId)) {
            $this->msg('推荐位ID无法获取！', false);
        }
        model('Position')->delData($posId);
        $this->msg('推荐位删除成功！');
    }
}