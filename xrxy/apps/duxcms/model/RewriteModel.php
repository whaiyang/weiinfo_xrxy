<?php
/**
 * RewriteModel.php
 * 伪静态规则操作
 * @author Life <349865361@qq.com>
 * @version 20140126
 */
class RewriteModel extends BaseModel
{
    protected $table = 'Rewrite';
    /**
     * 获取规则列表
     * @return array 规则列表
     */
    public function loadData()
    {
        $list = config('REWRITE');
        $data = array();
        if(!empty($list)){
            $i=1;
            foreach ($list as $key => $value) {
                $i++;
                $data[$i]['url']=html_in($key);
                $data[$i]['model']=$value;
            }
        }
        return $data;
    }
    /**
     * 保存配置
     * @param array $data 推荐信息
     * @return bool 状态
     */
    public function saveData($content) {
        // 写入配置
        $file = ROOT_PATH.'/inc/base/rewrite.ini.php';
        $content = "<?php \n//伪静态规则\n".$content;
        if (@file_put_contents($file, $content)){
            return true;
        }else{
           return false;
        }
    }
}