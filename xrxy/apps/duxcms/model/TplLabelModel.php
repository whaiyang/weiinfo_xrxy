<?php
/**
 * TplLabelModel.php
 * 模板基础标签操作
 * @author Life <349865361@qq.com>
 * @version 20140122
 */
class TplLabelModel extends BaseModel
{
    /**
     * 获取标签
     * @param array $data 标签信息
     * @return array 结果数组
     */
    public function getLabel($data)
    {
        if(empty($data['model'])&&empty($data['table'])){
            return array();
        }
        //转换模型标签
        if(!empty($data['model'])){
            $labelList=hook_api('apiLabel'.$data['model'],$data);
            $list=array();
            if(!empty($labelList)){
                foreach ($labelList as $value) {
                    $list=array_merge((array)$list,(array)$value);
                }
            }
            return $list;

        }
        //转换表查询
        if(!empty($data['table'])){
            switch ($data['type']) {
                case 'loop':
                default:
                    $list=$this->model->table($data['table'])->where($data['where'])->limit($data['limit'])->order($data['order'])->select();
                    break;
                case 'find':
                    $list=$this->model->table($data['table'])->where($data['where'])->limit($data['limit'])->order($data['order'])->find();
                    break;
                case 'count':
                    $list=$this->model->table($data['table'])->where($data['where'])->count();
                    break;
            }
            return $list;
        }
    }
}