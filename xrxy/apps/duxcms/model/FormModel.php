<?php
/**
 * FormModel.php
 * 表单操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FormModel extends BaseModel
{
    protected $table = 'form';
    /**
     * 获取表单列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 表单列表
     */
    public function loadData($condition = null, $limit = 100)
    {
        return $this->select($condition, '', 'form_id ASC', $limit);
    }
    /**
     * 获取表单总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 表单信息
     * @param int $formId 表单ID
     * @return array 用户信息
     */
    public function getInfo($formId)
    {
        return $this->find('form_id=' . $formId);
    }
    /**
     * 通过表名获取信息
     * @param int $formId 表单ID
     * @return array 用户信息
     */
    public function getInfoName($table)
    {
        return $this->find('`table`="' . $table .'"');
    }
    /**
     * 添加表单信息
     * @param array $data 表单信息
     * @return int 表单ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        //添加数据表
        $sql="
         CREATE TABLE IF NOT EXISTS `{$this->model->pre}form_{$data['table']}` (
          `data_id` int(10) NOT NULL AUTO_INCREMENT,
          `site` int(10) NOT NULL DEFAULT '1',
          PRIMARY KEY (`data_id`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ";
        $this->query($sql);
        //插入数据
        return $this->insert($data);
    }
    /**
     * 保存表单信息
     * @param array $data 表单信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        $info=$this->getInfo($data['form_id']);
        //修改模型表
        $sql="
        ALTER TABLE {$this->model->pre}form_{$info['table']} RENAME TO {$this->model->pre}form_{$data['table']}
        ";
        $this->model->query($sql);
        return $this->update('form_id=' . $data['form_id'], $data);
    }
    /**
     * 删除表单信息
     * @param int $formId 表单ID
     * @return bool 状态
     */
    public function delData($formId)
    {
        //删除表
        $info=$this->getInfo($formId);
        $sql="
        DROP TABLE `{$this->model->pre}form_{$info['table']}`
        ";
        $this->model->query($sql);
        return $this->delete('form_id=' . $formId);
    }
    /**
     * 查找重复信息
     * @param string $table 表名
     * @param string $formId 排除ID
     * @return array 用户信息
     */
    public function getInfoRepeat($table, $formId = null)
    {
        if ($formId) {
            return $this->count(('`table`="' . $table) . '" AND form_id<>' . $formId);
        } else {
            return $this->count(('`table`="' . $table) . '"');
        }
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data)
    {
        $formId=intval($data['form_id']);
        return $data;
    }
    /**
     * 获取表单菜单列表
     * @param array $data 录入数据
     * @return array 字段类型列表
     */
    public function loadFormMenu()
    {
        $list=$this->loadData();
        $data=array();
        if(!empty($list)){
            foreach ($list as $key=>$value) {
                $data[$key]['name']=$value['name'];
                $data[$key]['url']=url('AdminFormData/index',array('form_id'=>$value['form_id']));
            }
        }
        return $data;
    }
    /**
     * 获取表单HTML
     * @param array $data 录入数据
     * @return array 表单数组
     */
    public function getFormHtml($data)
    {
        if(empty($data['table'])){
            return array();
        }
        $info = $this->getInfoName($data['table']);
        if(empty($info)){
            return array();
        }
        //获取表单提交html
        $formHtml=model('FormData')->getField($info['form_id'],'',false);
        $postKey=getcode(32);
        $time=form_time();
        $formHtml.='<input name="post_key" type="hidden" value="'.$postKey.'">';
        $formHtml.='<input name="form" type="hidden" value="'.$data['table'].'">';
        $formHtml.='<input name="relation_key" type="hidden" value="'.$time.'">';
        $config=config('APP');
        $_SESSION[$config['COOKIE_PREFIX'].'form_'.$data['table']]=$postKey;
        return array('html'=>$formHtml, 'post_key'=>$postKey, 'relation_key'=>$time);
    }
    /**
     * 获取表单超链接
     * @param array $data 参数
     * @return string 表单链接
     */
    public function getFormUrl($data)
    {
        $parameter=array('name'=>$data['name']);
        $url = url('duxcms/Form/index',$parameter);
        return urldecode($url);
    }
}