<?php
/**
 * ReceptionModel.php
 * 前台通用模型
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class ReceptionModel extends BaseModel
{
    /**
     * 页面Meda信息组合
     * @param array $config 站点配置信息
     * @return array 用户信息
     */
    public function getMedia($config, $title='',$keywords='',$description='')
    {
        if(empty($title)){
            $title=$config['site_title'].' - '.$config['site_subtitle'];
        }else{
            $title=$title.' - '.$config['site_title'].' - '.$config['site_subtitle'];
        }
        if(empty($keywords)){
            $keywords=$config['site_keywords'];
        }
        if(empty($description)){
            $description=$config['site_description'];
        }
        return array(
            'title'=>$title,
            'keywords'=>$keywords,
            'description'=>$description,
        );
    }
}