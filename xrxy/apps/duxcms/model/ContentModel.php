<?php
/**
 * ContentModel.php
 * 内容表操作
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class ContentModel extends BaseModel
{
    protected $table = 'content';
    /**
     * 获取内容表列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 内容表列表
     */
    public function loadData($condition = null, $limit=20)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model->field('A.*,B.name as cname,B.app')->table('content', 'A')->leftJoin('category', 'B', array(
            'A.class_id',
            'B.class_id'
        ))->where($condition)->order('content_id DESC')->limit($limit)->select();
    }
    /**
     * 获取内容表总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='A.site = '.SITEID.' AND '.$condition;
        }else{
            $condition='A.site = '.SITEID;
        }
        return $this->model->field('A.*,B.name as cname')->table('content', 'A')->leftJoin('category', 'B', array(
            'A.class_id',
            'B.class_id'
        ))->where($condition)->count();
    }
    /**
     * 内容表信息
     * @param int $contentId 内容表ID
     * @return array 用户信息
     */
    public function getInfo($contentId)
    {
        return $this->find('site = '.SITEID.' AND content_id=' . $contentId);
    }
    /**
     * 添加内容表信息
     * @param array $data 内容表信息
     * @return int 内容表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        $contentId=$this->insert($data);
        //关联tag信息
        model('Tags')->relationData($data['keywords'], $contentId);
        //保存扩展信息
        $data['content_id']=$contentId;
        model('FieldData')->addData($data);
        return $contentId;
    }
    /**
     * 保存内容表信息
     * @param array $data 内容表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        //关联tag信息
        model('Tags')->relationData($data['keywords'], $data['content_id']);
        //保存扩展信息
        model('FieldData')->saveData($data);
        $newData = $data;
        unset($newData['content_id']);
        return $this->update('site = '.SITEID.' AND content_id in(' . $data['content_id'].')', $newData);
    }
    /**
     * 删除内容表信息
     * @param string $contentId 内容表ID
     * @return bool 状态
     */
    public function delData($contentId)
    {
        
        //删除tag信息
        model('Tags')->delRelationData($contentId);
        //删除扩展信息
        model('FieldData')->delData($contentId);
        //删除关联内容
        return $this->delete('site = '.SITEID.' AND content_id in(' . $contentId .')');
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $contentId=intval($data['content_id']);
        if(!empty($data['title'])){
            $data['urltitle']=$this->geturlTitle($data['title'],$data['urltitle'],$contentId);
        }
        $data['time']=strtotime($data['time']);
        if(empty($data['time'])){
            $data['time']=time();
        }
        if(!empty($data['type_top'])){
            $data['type_top']=strtotime($data['type_top']);
        }
        $data['taglink']=intval($data['taglink']);
        //提取描述
        if(empty($data['description'])&&!empty($data['content'])){
            $description=substr($data['content'], 0,1000);
            $description=html_out($description);
            $description=strip_tags($description);
            $description=str_replace(array("\r\n","\t",'&ldquo;','&rdquo;','&nbsp;'), '', $description);
            $data['description']=substr($description, 0,250);
        }
        $data['site'] = SITEID;
        return $data;
    }
    /**
     * 内容拼音转换
     * @param string $name 内容名称
     * @param string $urlTitle 内容拼音 可选
     * @param int $contentId 内容排除ID 可选
     * @return string 内容拼音
     */
    public function geturlTitle($name='', $urlTitle = null, $contentId = null)
    {
        if(empty($name)){
            return false;
            exit;
        }
        if (empty($urlTitle))
        {
            $pinyin = new Pinyin();
            $name = preg_replace('/\s+/', '-', $name);
            $pattern = '/[^\x{4e00}-\x{9fa5}\d\w\-]+/u';
            $name = preg_replace($pattern, '', $name);
            $urlTitle = substr($pinyin->output($name, true),0,30);
            if(substr($urlTitle,0,1)=='-'){
                $urlTitle=substr($urlTitle,1);
            }
            if(substr($urlTitle,-1)=='-'){
                $urlTitle=substr($urlTitle,0,-1);
            }
        }

        $where='';
        if (!empty($contentId))
        {
            $where = 'AND content_id<>' . $contentId;
        }
        
        $info = $this->count("urltitle='".$urlTitle."'" .$where); 

        if (empty($info))
        {
            return $urlTitle;
        }
        else
        {
            return $urlTitle.substr(cp_uniqid(),8);
        }
    }
}