<?php
/**
 * FieldsetModel.php
 * 字段集表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FieldsetModel extends BaseModel
{
    protected $table = 'fieldset';
    /**
     * 获取字段集列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 字段集列表
     */
    public function loadData($condition = null, $limit = 100)
    {
        return $this->select($condition, '', 'fieldset_id ASC', $limit);
    }
    /**
     * 获取字段集总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 字段集信息
     * @param int $fieldsetId 字段集ID
     * @return array 用户信息
     */
    public function getInfo($fieldsetId)
    {
        return $this->find('fieldset_id=' . $fieldsetId);
    }
    /**
     * 添加字段集信息
     * @param array $data 字段集信息
     * @return int 字段集ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        //添加数据表
        $sql="
        CREATE TABLE IF NOT EXISTS `{$this->model->pre}fieldset_{$data['table']}` (
          `content_id` int(10) DEFAULT NULL
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
        ";
        $this->query($sql);
        //插入数据
        return $this->insert($data);
    }
    /**
     * 保存字段集信息
     * @param array $data 字段集信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        $info=$this->getInfo($data['fieldset_id']);
        //修改模型表
        $sql="
        ALTER TABLE {$this->model->pre}fieldset_{$info['table']} RENAME TO {$this->model->pre}fieldset_{$data['table']}
        ";
        $this->model->query($sql);
        return $this->update('fieldset_id=' . $data['fieldset_id'], $data);
    }
    /**
     * 删除字段集信息
     * @param int $fieldsetId 字段集ID
     * @return bool 状态
     */
    public function delData($fieldsetId)
    {
        //删除表
        $info=$this->getInfo($fieldsetId);
        $sql="
        DROP TABLE `{$this->model->pre}fieldset_{$info['table']}`
        ";
        $this->model->query($sql);
        return $this->delete('fieldset_id=' . $fieldsetId);
    }
    /**
     * 查找重复信息
     * @param string $table 表名
     * @param string $fieldsetId 排除ID
     * @return array 用户信息
     */
    public function getInfoRepeat($table, $fieldsetId = null)
    {
        if ($fieldsetId) {
            return $this->count(('`table`="' . $table) . '" AND fieldset_id<>' . $fieldsetId);
        } else {
            return $this->count(('`table`="' . $table) . '"');
        }
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $fieldsetId=intval($data['fieldset_id']);
        return $data;
    }

    /**
     * 判断字段集录入
     * @param array $data 录入数据
     * @return array 字段类型列表
     */
    public function checkField($data)
    {
        $classId=$data['class_id'];
        if(empty($classId)){
            return;
        }
        //获取栏目信息
        $classInfo=model('Category')->getInfo($classId);
        if(empty($classInfo)||!$classInfo['fieldset_id']){
            return;
        }
        //获取字段列表
        $fieldsetInfo=model('Fieldset')->getInfo($classInfo['fieldset_id']);
        if(empty($fieldsetInfo)){
            return;
        }
        $fieldList=model('Field')->loadData('fieldset_id='.$classInfo['fieldset_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        foreach ($fieldList as $value) {
            if(!DuxForm::check($data['Fieldset_'.$value['field']],unserialize($value['datatype']))){
                return array($value['errormsg']);
            }
        }
        

    }
}