<?php
/**
 * FragmentModel.php
 * 碎片表操作
 * @author Life <349865361@qq.com>
 * @version 20140126
 */
class FragmentModel extends BaseModel
{
    protected $table = 'fragment';
    /**
     * 获取碎片列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 碎片列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->select($condition, '', 'fragment_id ASC', $limit);
    }
    /**
     * 获取碎片总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->count($condition);
    }
    /**
     * 碎片信息
     * @param int $fragmentId 碎片ID
     * @return array 用户信息
     */
    public function getInfo($fragmentId)
    {
        return $this->find('site = '.SITEID.' AND fragment_id=' . $fragmentId);
    }
    /**
     * 添加碎片信息
     * @param array $data 碎片信息
     * @return int 碎片ID
     */
    public function addData($data)
    {
        $data['site'] = SITEID;
        return $this->insert($data);
    }
    /**
     * 保存碎片信息
     * @param array $data 碎片信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('site = '.SITEID.' AND fragment_id=' . $data['fragment_id'], $data);
    }
    /**
     * 删除碎片信息
     * @param int $fragmentId 碎片ID
     * @return bool 状态
     */
    public function delData($fragmentId)
    {
        return $this->delete('site = '.SITEID.' AND fragment_id=' . $fragmentId);
    }
    /**
     * 碎片内容标签
     * @param array $data 标签信息
     * @return array TAG列表
     */
    public function getLabel($data)
    {
        if(empty($data['id'])){
            return '';
        }
        $info=$this->getInfo($data['id']);
        $data=html_out($info['content']);
        return $data;
    }
}