<?php
/**
 * FormDataModel.php
 * 表单数据表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FormDataModel extends BaseModel
{
    protected $table = 'form_data';
    /**
     * 获取表单数据列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 表单数据列表
     */
    public function loadData($table, $condition = null,$order = 'data_id DESC', $limit = 20)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        $this->table='form_'.$table;
        return $this->select($condition, '', $order, $limit);
    }
    /**
     * 获取表单数据总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($table, $condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        $this->table='form_'.$table;
        return $this->count($condition);
    }
    /**
     * 表单数据信息
     * @param int $dataId 表单数据ID
     * @return array 用户信息
     */
    public function getInfo($table, $dataId)
    {
        $this->table='form_'.$table;
        return $this->find('site = '.SITEID.' AND  data_id=' . $dataId);
    }
    /**
     * 添加表单数据信息
     * @param array $data 表单数据信息
     * @return int 表单数据ID
     */
    public function addData($post)
    {
        //格式化部分字段
        $data=$this->foramtData($post);
        $dataId=$this->insert($data);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$dataId,'model'=>$this->table,'relation_key'=>$post['relation_key']));
        return $dataId;
    }
    /**
     * 保存表单数据信息
     * @param array $data 表单数据信息
     * @return bool 状态
     */
    public function saveData($post)
    {
        //格式化部分字段
        $data=$this->foramtData($post);
        $status=$this->update('site = '.SITEID.' AND data_id=' . $data['data_id'], $data);
        //关联附件
        api('admin','relationFile',array('relation_id'=>$data['data_id'],'model'=>$this->table,'relation_key'=>$post['relation_key']));
        return $status;
    }
    /**
     * 删除表单数据信息
     * @param int $dataId 表单数据ID
     * @return bool 状态
     */
    public function delData($table, $dataId)
    {
        $this->table='form_'.$table;
        $status=$this->delete('site = '.SITEID.' AND data_id=' . $dataId);
        //关联附件
        api('admin','delRelationFile',array('relation_id'=>$dataId,'model'=>$this->table));
        return $status;
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        if(empty($data['form_id'])){
            return;
        }
        $formInfo=model('Form')->getInfo($data['form_id']);
        if(empty($formInfo)){
            return;
        }
        $this->table='form_'.$formInfo['table'];
        $fieldList=model('FormField')->loadData('form_id='.$data['form_id']);
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        $newData=array();
        $newData['data_id']=$data['data_id'];
        foreach ($fieldList as $value) {
            $newData[$value['field']]=model('Field')->formatField($data[$value['field']],$value['type']);
        }
        $newData['site'] = SITEID;
        return $newData;
    }
    /**
     * 获取表单元素
     * @param int $formId 表单ID
     * @param int $dataId 数据ID
     * @return string 表单html
     */
    public function getField($formId,$dataId='',$show=true)
    {
        $formInfo=model('Form')->getInfo($formId);
        //字段列表
        $fieldList=model('FormField')->loadData('form_id='.$formId);
        if(empty($fieldList)){
            return;
        }
        $typeField=model('Field')->typeField();
        //获取内容信息
        if(!empty($dataId)){
            $contentInfo=$this->getInfo($formInfo['table'],$dataId);
        }
        $html='';
        $form=new DuxForm();
        foreach ($fieldList as $value) {
            if(!$show){
                if (!$value['delivery']) {
                    continue;
                }
            }
            $method=$typeField[$value['type']]['html'];
            $config=array();
            $config['title']=$value['name'];
            $config['name']=$value['field'];
            if($contentInfo[$value['field']]){
                $config['value']=$contentInfo[$value['field']];
            }else{
                $config['value']=$value['default'];
            }
            $config['size']=$value['size'];
            $config['datatype']=unserialize($value['datatype']);
            $config['tip']=$value['tip'];
            $config['errormsg']=$value['errormsg'];
            $list=explode(',', $value['config']);
            $newList=array();
            if(!empty($list)){
                foreach ($list as $key => $value) {
                    $newList[$key]['value']=$key+1;
                    $newList[$key]['title']=$value;
                }
            }
            $config['list']=$newList;
            $html.=$form->$method($config);
        }
        return $html;
    }
    /**
     * 判断数据录入
     * @param array $data 录入数据
     * @return array 字段类型列表
     */
    public function checkField($data)
    {
        $formId=$data['form_id'];
        //获取字段列表
        $fieldList=model('FormField')->loadData('form_id='.$formId);
        if(empty($fieldList)||!is_array($fieldList)){
            return '没有发现字段';
        }
        foreach ($fieldList as $value) {
            if(!DuxForm::check($data[$value['field']],unserialize($value['datatype']))){
                if(!empty($value['errormsg'])){
                    return $value['errormsg'];
                }else{
                    return $value['name'].'输入不正确！';
                }
            }
        }
    }
    /**
     * 表单内容列表标签
     * @param array $data 标签信息
     * @return array TAG列表
     */
    public function loadLabelList($data)
    {
        if(empty($data['table'])){
            return array();
        }
        $info = model('Form')->getInfoName($data['table']);
        if(empty($info)){
            return array();
        }
        //获取表单字段
        $fieldList=model('FormField')->loadData('form_id='.$info['form_id']);
        if(empty($fieldList)){
            return array();
        }
        if(!empty($data['order'])){
            $data['order']=$data['order'].',';
        }
        if(empty($data['limit'])){
            $data['limit']=10;
        }
        $where=$info['where'].','.$data['where'];
        $where=trim($where,',');
        $list = $this->loadData($data['table'], $where, $data['order'], $data['limit']);
        $data = array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $data[$key] = $value;
                foreach ($fieldList as $v) {
                    $data[$key][$v['field']]=model('FormField')->showField($value[$v['field']],$v['type'],$v['config'],t);
                }
            }
        }
        return $data;
    }
}