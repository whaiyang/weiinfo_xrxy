<?php
/**
 * CategoryModel.php
 * 栏目表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class CategoryModel extends BaseModel
{
    protected $table = 'category';
    /**
     * 获取栏目表列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 栏目表列表
     */
    public function loadData($condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->select($condition, '', 'sequence ASC,class_id ASC');
    }
    /**
     * 获取栏目分类数
     * @param array $list 条件
     * @return array 栏目树列表
     */
    public function getTrueData($condition = null, $classId=0)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        $data=$this->select($condition, '', 'sequence ASC,class_id ASC');
        $cat = new Category(array('class_id', 'parent_id', 'name', 'cname'));
        $data=$cat->getTree($data, intval($classId));
        return $data;
    }
    /**
     * 获取栏目表总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        if(!empty($condition)){
            $condition='site = '.SITEID.' AND '.$condition;
        }else{
            $condition='site = '.SITEID;
        }
        return $this->count($condition);
    }
    /**
     * 栏目表信息
     * @param int $classId 栏目表ID
     * @return array 用户信息
     */
    public function getInfo($classId)
    {
        return $this->find('site = '.SITEID.' AND class_id=' . $classId);
    }
    /**
     * 通过URL获取栏目信息
     * @param string $urlName URL信息
     * @return array 用户信息
     */
    public function getInfoUrl($urlName)
    {
        return $this->find('site = '.SITEID.' AND urlname="' . $urlName .'"');
    }
    /**
     * 添加栏目表信息
     * @param array $data 栏目表信息
     * @return int 栏目表ID
     */
    public function addData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        $classId=$this->insert($data);
        //添加菜单
        $menuData=array();
        $menuData['parent_id']=$data['parent_id'];
        $menuData['class_id']=$classId;
        model('Catalog')->addDataClass($menuData);
        return $classId;
    }
    /**
     * 保存栏目表信息
     * @param array $data 栏目表信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        //格式化部分字段
        $data=$this->foramtData($data);
        $status = $this->update('site = '.SITEID.' AND class_id=' . $data['class_id'], $data);
        //编辑菜单
        if($data['upmenu']){
            $menuData=array();
            $menuData['parent_id']=$data['parent_id'];
            $menuData['class_id']=$data['class_id'];
            model('Catalog')->editDataClass($menuData);
        }
        return $status;
    }
    /**
     * 删除栏目表信息
     * @param int $classId 栏目表ID
     * @return bool 状态
     */
    public function delData($classId)
    {
        //删除关联内容
        $status = $this->delete('site = '.SITEID.' AND class_id=' . $classId);
        //删除菜单
        model('Catalog')->delDataClass($classId);
        return $status;
    }
    /**
     * 数据格式化
     * @param array $data 栏目信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $classId=intval($data['class_id']);
        if(!empty($data['name'])){
            $data['urlname']=$this->getUrlName($data['name'],$data['urlname'],$classId);
        }
        $menuData['site'] = SITEID;
        return $data;
    }
    /**
     * 栏目拼音转换
     * @param string $name 栏目名称
     * @param string $urlName 栏目拼音 可选
     * @param int $classId 栏目排除ID 可选
     * @return string 栏目拼音
     */
    public function getUrlName($name='', $urlName = null, $classId = null)
    {
        if(empty($name)){
            return false;
            exit;
        }
        if (empty($urlName))
        {
            $pinyin = new Pinyin();
            $name = preg_replace('/\s+/', '-', $name);
            $pattern = '/[^\x{4e00}-\x{9fa5}\d\w\-]+/u';
            $name = preg_replace($pattern, '', $name);
            $urlName = substr($pinyin->output($name, true),0,30);
            if(substr($urlName,0,1)=='-'){
                $urlName=substr($urlName,1);
            }
            if(substr($urlName,-1)=='-'){
                $urlName=substr($urlName,0,-1);
            }
        }

        $where='';
        if (!empty($classId))
        {
            $where = 'AND class_id<>' . $classId;
        }
        
        $info = $this->count("urlname='".$urlName."'" .$where); 

        if (empty($info))
        {
            return $urlName;
        }
        else
        {
            return $urlName.substr(cp_uniqid(),8);
        }
    }
}