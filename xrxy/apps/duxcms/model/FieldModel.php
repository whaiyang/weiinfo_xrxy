<?php
/**
 * FieldModel.php
 * 字段表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class FieldModel extends BaseModel
{
    protected $table = 'field';
    /**
     * 获取字段列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 字段列表
     */
    public function loadData($condition = null)
    {
        return $this->select($condition, '', 'sequence ASC,field_id ASC');
    }
    /**
     * 获取字段总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 字段信息
     * @param int $fieldId 字段ID
     * @return array 用户信息
     */
    public function getInfo($fieldId)
    {
        return $this->find('field_id=' . $fieldId);
    }
    /**
     * 添加字段信息
     * @param array $data 字段信息
     * @return int 字段ID
     */
    public function addData($data)
    {
        $fieldsetInfo=model('Fieldset')->getInfo($data['fieldset_id']);
        //获取字段属性
        $typeField = model('Field')->typeField();
        $propertyField = model('Field')->propertyField();
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //添加真实字段
        $sql="
        ALTER TABLE {$this->model->pre}fieldset_{$fieldsetInfo['table']} ADD {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']}) DEFAULT NULL
        ";
        $this->query($sql);
        //格式化部分字段
        $data=$this->foramtData($data);
        //插入数据
        return $this->insert($data);
    }
    /**
     * 保存字段信息
     * @param array $data 字段信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        $info=$this->getInfo($data['field_id']);
        $fieldsetInfo=model('Fieldset')->getInfo($data['fieldset_id']);
        //获取字段属性
        $typeField = model('Field')->typeField();
        $propertyField = model('Field')->propertyField();
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //修改真实字段
        if($info['type']<>$data['type']||$info['field']<>$data['field']){
            $sql="
            ALTER TABLE {$this->model->pre}fieldset_{$fieldsetInfo['table']} CHANGE {$info['field']} {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']})
            ";
            $this->query($sql);
        }
        //格式化部分字段
        $data=$this->foramtData($data);
        //修改数据
        return $this->update('field_id=' . $data['field_id'], $data);
    }
    /**
     * 删除字段信息
     * @param int $fieldId 字段ID
     * @return bool 状态
     */
    public function delData($fieldId)
    {
        //删除表
        $info=$this->getInfo($fieldId);
        $fieldsetInfo=model('Fieldset')->getInfo($info['fieldset_id']);
        $sql="
             ALTER TABLE {$this->model->pre}fieldset_{$fieldsetInfo['table']} DROP {$info['field']}
            ";
        $this->model->query($sql);
        return $this->delete('field_id=' . $fieldId);
    }
    /**
     * 删除字段信息通过字段集
     * @param int $fieldId 字段ID
     * @return bool 状态
     */
    public function delDataFieldset($fieldsetId)
    {
        return $this->delete('fieldset_id=' . $fieldsetId);
    }
    /**
     * 查找重复信息
     * @param string $field 字段名
     * @param string $fieldsetId 字段ID
     * @param string $fieldId 排除ID
     * @return array 用户信息
     */
    public function getInfoRepeat($field, $fieldsetId, $fieldId = null)
    {
        if ($fieldId) {
            return $this->count(('`field`="' . $field) . '" AND field_id<>' . $fieldId .' AND fieldset_id='.$fieldsetId);
        } else {
            return $this->count(('`field`="' . $field) . '" AND fieldset_id='.$fieldsetId);
        }
    }
    /**
     * 数据格式化
     * @param array $data 内容信息
     * @return array 格式化后信息
     */
    public function foramtData($data){
        $data['datatype']=serialize($data['datatype']);
        return $data;
    }
    /**
     * 获取字段类型
     * @param int $type 字段类型
     * @return array 字段类型列表
     */
    public function typeField()
    {
        $list=array(
            1=> array(
                'name'=>'文本框',
                'property'=>1,
                'html'=>'text',
                ),
            2=> array(
                'name'=>'多行文本',
                'property'=>3,
                'html'=>'textarea',
                ),
            3=> array(
                'name'=>'编辑器',
                'property'=>3,
                'html'=>'editor',
                ),
            4=> array(
                'name'=>'文件上传',
                'property'=>1,
                'html'=>'fileUpload',
                ),
            5=> array(
                'name'=>'单图片上传',
                'property'=>1,
                'html'=>'imgUpload',
                ),
            6=> array(
                'name'=>'多图上传',
                'property'=>3,
                'html'=>'imagesUpload',
                ),
            7=> array(
                'name'=>'下拉菜单',
                'property'=>3,
                'html'=>'select',
                ),
            8=> array(
                'name'=>'单选',
                'property'=>3,
                'html'=>'radio',
                ),
            9=> array(
                'name'=>'多选',
                'property'=>3,
                'html'=>'checkbox',
                ),
            10=> array(
                'name'=>'日期和时间',
                'property'=>2,
                'html'=>'textTime',
                ),
            11=> array(
                'name'=>'货币',
                'property'=>4,
                'html'=>'text',
                ),
            12=> array(
                'name'=>'地区联动',
                'property'=>1,
                'html'=>'area',
                ),
            
        );
        return $list;
    }
    /**
     * 获取SQL字段属性
     * @param int $type 字段类型
     * @return array 字段类型列表
     */
    public function propertyField()
    {
        $list=array(
            1=> array(
                'name'=>'varchar',
                'maxlen'=>250,
                'decimal'=>0,
                ),
            2=> array(
                'name'=>'int',
                'maxlen'=>10,
                'decimal'=>0,
                ),
            3=> array(
                'name'=>'text',
                'maxlen'=>0,
                'decimal'=>0,
                ),
            4=> array(
                'name'=>'decimal',
                'maxlen'=>10,
                'decimal'=>0,
                ),
        );
        return $list;
    }
    /**
     * 格式化字段属性
     * @param int $type 字段类型
     * @return array 字段类型列表
     */
    public function formatField($data,$type)
    {
        switch ($type) {
            case '1':
            case '4':
            case '5':
                return in($data);
                break;
            case '2':
            case '3':
                return html_in($data);
                break;
            case '6':
                //修改文件名称
                if(is_array($data['id'])){
                    foreach ($data['id'] as $key => $value) {
                        $fileData=array();
                        $fileData['title']=$data['title'][$key];
                        $this->model->table('attachment')->data($fileData)->where('file_id='.$value)->update();
                    }
                    return implode(',',$data['id']);
                }
                break;
            case '7':
            case '8':
                return intval($data);
                break;
            case '10':
                if(!empty($data)){
                    return strtotime($data);
                }else{
                    return time();
                }
                break;
            case '9':
            case '12':
                if(!empty($data)&&is_array($data)){
                    return implode(',',$data);
                }
                break;
            default:
                return in($data);
                break;
        }
    }
    /**
     * 字段列表显示
     * @param int $type 字段类型
     * @return array 字段类型列表
     */
    public function showField($data,$type,$config)
    {
        switch ($type) {
            case '1':
            case '2':
            case '3':
            case '4':
                return $data;
                break;
            case '5':
                if($data){
                    return '<img name="" src="'.$data.'" alt="" style="max-width:170px; max-height:90px; _width:170px; _height:90px;" />';
                }else{
                    return '无';
                }
                break;
            case '6':
                //文件列表
                if(empty($data)){
                    return '无';
                }
                $list=api('admin','getFileList',array('file_id'=>$data));
                $html='';
                if(!empty($list)){
                    foreach ($list as $key => $value) {
                        $html.=$value['url'].'<br>';
                    }
                }
                return $html;
                break;
            case '7':
            case '8':
                if(empty($config)){
                    return $data;
                }
                $list=explode(",",trim($config));
                foreach ($list as $key => $vo) {
                    if($data==intval($key)+1){
                        return $vo;
                    }
                }
                break;
            case '10':
                return date('Y-m-d H:i:s',$data);
                break;
            case '9':
                if(empty($config)){
                    return $data;
                }
                $list=explode(",",trim($config));
                $html='';
                foreach ($list as $key => $vo) {
                    if($data==intval($key)){
                        $html.=' '.$vo.' |';
                    }
                }
                return substr($html,0,-1);
                break;
            case '12':
                if(!empty($data)){
                    return model('Region')->getName($data);
                }else{
                    return '未设置';
                }
                break;
            default:
                return in($data);
                break;
        }
    }
}