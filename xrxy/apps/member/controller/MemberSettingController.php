<?php
/**
 * MemberSettingController.php
 * 会员设置
 * @author Life <349865361@qq.com>
 * @version 20140318
 */
class MemberSettingController extends UserController
{
    /**
     * 基本资料
     */
    public function info()
    {
        $userInfo = model('Member')->getInfo($this->userId);
        $expand=model('MemberField')->getField($this->userId);
        //模板赋值
        $this->assign('userInfo', $userInfo);
        $this->assign('expand', $expand);
        $this->show();
    }
    /**
     * 修改基本资料数据
     */
    public function saveInfo()
    {
        $data=in($_POST);
        $checkField=model('MemberField')->checkField($data);
        if(!empty($checkField)){
            $this->msg($checkField, false);
        }
        $data['user_id'] = $this->userId;
        model('MemberData')->saveData($data);
        $this->msg('资料修改成功！', 1);
    }
    /**
     * 设置头像
     */
    public function avatar()
    {
        $userInfo = model('Member')->getInfo($this->userId);
        //模板赋值
        $this->assign('userInfo', $userInfo);
        $this->assign('expand', $expand);
        $this->show();
    }
    /**
     * 上传头像
     */
    public function saveAvatar()
    {
        $dir=ROOT_PATH.'/apps/member/avatar/'.$this->userId.'/';
        if(!file_exists($dir)){
          mkdir($dir, 0777, true);
        }
        switch($_GET['action']){
            case 'uploadtmp':
                $file=$dir.'original.jpg';
                @move_uploaded_file($_FILES['Filedata']['tmp_name'], $file);
                $status = 1;
            break;
            default:
                $input = file_get_contents('php://input');
                $data = explode('--------------------', $input);
                if(!empty($data)){
                    @file_put_contents($dir.'original.jpg', $data[1]);
                    @file_put_contents($dir.'large.jpg', $data[0]);
                    @Image::thumb($dir.'large.jpg', $dir.'moderate.jpg', '', 64, 64, true,false);
                    @Image::thumb($dir.'large.jpg', $dir.'small.jpg', '', 32, 32, true,false);
                    $status = 1;
                }
            break;
        }
        $this->msg('头像保存成功！',$status, true);
    }
    /**
     * 修改密码
     */
    public function password()
    {
        $userInfo = model('Member')->getInfo($this->userId);
        //模板赋值
        $this->assign('userInfo', $userInfo);
        $this->show();
    }
    /**
     * 修改基本资料数据
     */
    public function passwordData()
    {
        $data=in($_POST);
        if(!Check::userName($data['password'],6,250,'/[\w\W]+/')||empty($data['password'])){
            $this->msg('密码最少需要6位字符',0);
        }
        if($data['password']<>$data['password2']){
            $this->msg('两次密码输入不同！',0);
        }
        $userInfo = model('Member')->getInfo($this->userId);
        if($userInfo['password']<>md5($data['old_password'])){
            $this->msg('原始密码输入不正确！',0);
        }
        $newData = array();
        $newData['user_id'] = $this->userId;
        $newData['password'] = md5($data['password']);
        model('Member')->saveData($newData);
        $this->msg('密码修改成功！', 1);
    }
    
}