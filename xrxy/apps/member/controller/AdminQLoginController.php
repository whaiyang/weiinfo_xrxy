<?php
/**
 * AdminQLoginController.php
 * QQ登录设置
 * @author Life <349865361@qq.com>
 * @version 20140226
 */
class AdminQLoginController extends AdminController
{
    /**
     * 编辑
     */
    public function index()
    {
        $config = appConfig('member');
        $info = $config['OAUTH_QQ'];
        //模板赋值
        $this->assign('info', $info);
        $this->show('adminmodel/info');
    }
    /**
     * 处理
     */
    public function saveData()
    {
        unset($_POST['site']);
        unset($_POST['relation_key']);
        $config = appConfig('member');
        $info = $config['OAUTH_QQ'];
        $data['OAUTH_QQ']=in($_POST);
        $data['OAUTH_QQ']=array_merge($info,$data['OAUTH_QQ']);
        $data['OAUTH_QQ']['STATUS'] = intval($data['OAUTH_QQ']['STATUS']);
        if( save_config('member', $data)){
            $this->msg('模型设置成功！');
        }else{
            $this->msg('模型设置失败，APP目录无写入权限！');
        }
    }
}