<?php
/**
 * OauthLoginController.php
 * Oauth登录相关
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class OauthLoginController extends UserController
{
    public function __construct(){
        parent::__construct();
        $this->cookiePrefix = $this->appConfig['COOKIE_PREFIX'];
    }
    /**
     * 判断session信息
     */
    public function checkSession()
    {
        //获取配置信息
        if(empty($_SESSION[$this->cookiePrefix.'member_oauth_id'])||empty($_SESSION[$this->cookiePrefix.'member_oauth_app'])||empty($_SESSION[$this->cookiePrefix.'member_oauth_mark'])||empty($_SESSION[$this->cookiePrefix.'member_oauth_url'])){
            $this->msg('暂时无法获取到您的登录信息！');
        }
    }
    /**
     * 登录页面
     */
    public function index()
    {
        $this->cookiePrefix = $this->appConfig['COOKIE_PREFIX'];
        $this->checkSession();
        //获取配置信息
        $config = appConfig($_SESSION[$this->cookiePrefix.'member_oauth_app']);
        $config = $config['OAUTH_'.$_SESSION[$this->cookiePrefix.'member_oauth_mark']];

        if(empty($config)){
            $this->msg('暂时无法获取到您的登录配置信息！');
        }
        //模板赋值
        $this->assign('loginUrl', url($_SESSION[$this->cookiePrefix.'member_oauth_url'].'/index'));
        $this->assign('callbackUrl', url($_SESSION[$this->cookiePrefix.'member_oauth_url'].'/callback'));
        $this->assign('oauthConfig', $config);
        $this->showFrame();
    }
    /**
     * 绑定帐号
     */
    public function bindLogin()
    {
        $this->cookiePrefix = $this->appConfig['COOKIE_PREFIX'];
        $this->checkSession();
        $data=in($_POST);
        $oauthId = $_SESSION[$this->cookiePrefix.'member_oauth_id'];
        $oauthMark = $_SESSION[$this->cookiePrefix.'member_oauth_mark'];
        //获取配置信息
        if(empty($data['username'])||empty($data['password'])){
            $this->msg('请填写完整登录信息!',0);
        }
        //获取帐号信息
        $info=model('Member')->getRepeat('username',$data['username']);
        if(empty($info)){
            $info=model('Member')->getRepeat('email',$data['username']);
        }
        //进行帐号验证
        if(empty($info)){
            $this->msg('绑定失败! 您输入的帐号不存在!',0);
        }
        if($info['password']<>md5($data['password'])){
            $this->msg('绑定失败! 您输入的帐号或密码错误!',0);
        }
        if($info['status']==0){
            $this->msg('绑定失败! 您已被禁止登录!',0);
        }
        $oauthInfo = model('MemberOauth')->getInfo('user_id = "'.$info['user_id'].'" AND mark = "'.$oauthMark.'"');
        if(!empty($oauthInfo)){
            $this->msg('绑定失败! 您已经绑定过该帐号!',0);
        }else{
            $data = array();
            $data['user_id'] = $info['user_id'];
            $data['open_id'] = $oauthId;
            $data['mark'] = $oauthMark;
            model('MemberOauth')->addData($data);
            $this->msg('绑定帐号成功！');
        }
        
    }
    /**
     * 设置登录信息
     */
    public function loginData()
    {
        $oauthId = $_SESSION[$this->cookiePrefix.'member_oauth_id'];
        $oauthMark = $_SESSION[$this->cookiePrefix.'member_oauth_mark'];
        if(empty($oauthId)||empty($oauthMark)){
            $this->error404();
        }
        $oauthInfo = model('MemberOauth')->getInfo('open_id = "'.$oauthId.'" AND mark = "'.$oauthMark.'"');
        if(empty($oauthInfo)){
            $this->error404();
        }
        $data=array();
        $data['user_id']=$oauthInfo['user_id'];
        $data['last_time']=time();
        $data['last_ip']=get_client_ip();
        model('Member')->saveData($data);
        $expire = time() + 7200;
        $cookie=$oauthInfo['user_id'];
        $this->setLogin($cookie,$expire);
        unset($_SESSION[$this->cookiePrefix.'member_token']);
        unset($_SESSION[$this->cookiePrefix.'member_oauth_id']);
        unset($_SESSION[$this->cookiePrefix.'member_oauth_mark']);
        $this->redirect(url('member/Index/index'));
    }
}