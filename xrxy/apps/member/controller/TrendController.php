<?php
/**
 * TrendController.php
 * 内容管理
 * @author Life <349865361@qq.com>
 * @version 20140318
 */
class TrendController extends UserController
{
    /**
     * 列表页
     */
    public function index()
    {
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => '{page}'
        );
        $url = url('Trend/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'user_id = '.$this->userId;
        //列表信息
        $list = model('MemberTrend')->loadData($where, $limit);
        $count = model('MemberTrend')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
}