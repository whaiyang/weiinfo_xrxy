<?php 
return array (
  'APP_STATE' => 0,
  'APP_NAME' => '用户中心',
  'APP_VER' => '20140310',
  'APP_AUTHOR' => 'Life',
  'APP_ORIGINAL_PREFIX' => 'dux_',
  'APP_TABLES' => 'member,member_bulletin,member_data,member_field,member_group,member_role,member_msg,member_msg_has,member_oauth,member_trend',
  'APP_DESCRIPTION' => '会员基础系统',
  'APP_SYSTEM' => 0,
  'APP_INSTALL' => 0,
  'APP_SETTING' => 
  array (
    'SITE_TITLE' => 'DuxCms 用户中心',
    'REG_STATUS' => 1,
    'REG_GROUP' => '4',
  ),
  'APP_EMAIL' => 
  array (
    'SMTP_HOST' => 'smtp.qq.com',
    'SMTP_PORT' => 25,
    'SMTP_SSL' => 0,
    'SMTP_AUTH' => 1,
    'SMTP_USERNAME' => 'admin@duxcms.com',
    'SMTP_PASSWORD' => '123456',
    'SMTP_FROM_NAME' => 'DuxCms 官方',
  ),
  'OAUTH_QQ' => 
  array (
    'STATUS' => 1,
    'ID' => '12345',
    'KEY' => '12345',
    'NAME' => 'QQ帐号',
    'MARK' => 'QQ',
  ),
);