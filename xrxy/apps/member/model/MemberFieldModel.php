<?php
/**
 * MemberFieldModel.php
 * 会员字段表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberFieldModel extends BaseModel
{
    protected $table = 'member_field';
    /**
     * 获取会员字段列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 会员字段列表
     */
    public function loadData($condition = null)
    {
        return $this->select($condition, '', 'sequence ASC,field_id ASC');
    }
    /**
     * 获取会员字段总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 会员字段信息
     * @param int $fieldId 会员字段ID
     * @return array 用户信息
     */
    public function getInfo($fieldId)
    {
        return $this->find('field_id=' . $fieldId);
    }
    /**
     * 添加会员字段信息
     * @param array $data 会员字段信息
     * @return int 会员字段ID
     */
    public function addData($data)
    {
        //获取会员字段属性
        $typeField = api('duxcms','getFieldType');
        $propertyField = api('duxcms','getFieldProperty');
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //添加真实会员字段
        $sql="
        ALTER TABLE {$this->model->pre}member_data ADD {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']}) DEFAULT NULL
        ";
        $this->query($sql);
        $data['datatype']=serialize($data['datatype']);
        //插入数据
        return $this->insert($data);
    }
    /**
     * 保存会员字段信息
     * @param array $data 会员字段信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        $info=$this->getInfo($data['field_id']);
        //获取会员字段属性
        $typeField = api('duxcms','getFieldType');
        $propertyField = api('duxcms','getFieldProperty');
        $type = $typeField[$data['type']];
        $property = $propertyField[$type['property']];
        if($property['decimal']){
            $property['decimal']=','.$property['decimal'];
        }else{
            $property['decimal']='';
        }
        //修改真实会员字段
        if($info['type']<>$data['type']||$info['field']<>$data['field']){
            $sql="
            ALTER TABLE {$this->model->pre}member_data CHANGE {$info['field']} {$data['field']} {$property['name']}({$property['maxlen']}{$property['decimal']})
            ";
            $this->query($sql);
        }
        $data['datatype']=serialize($data['datatype']);
        //修改数据
        return $this->update('field_id=' . $data['field_id'], $data);
    }
    /**
     * 删除会员字段信息
     * @param int $fieldId 会员字段ID
     * @return bool 状态
     */
    public function delData($fieldId)
    {
        //删除表
        $info=$this->getInfo($fieldId);
        $sql="
             ALTER TABLE {$this->model->pre}member_data DROP {$info['field']}
            ";
        $this->model->query($sql);
        return $this->delete('field_id=' . $fieldId);
    }
    /**
     * 查找重复信息
     * @param string $field 字段名
     * @param string $formId 会员ID
     * @param string $fieldId 排除ID
     * @return array 用户信息
     */
    public function getInfoRepeat($field, $fieldId = null)
    {
        if ($fieldId) {
            return $this->count(('`field`="' . $field) . '" AND field_id<>' . $fieldId);
        } else {
            return $this->count(('`field`="' . $field .'"'));
        }
    }


    /**
     * 判断数据录入
     * @param array $data 录入数据
     * @return array 字段类型列表
     */
    public function checkField($data)
    {
        //获取字段列表
        $fieldList=$this->loadData();
        if(empty($fieldList)||!is_array($fieldList)){
            return;
        }
        foreach ($fieldList as $value) {
            if(!DuxForm::check($data[$value['field']],unserialize($value['datatype']))){
                return $value['errormsg'];
            }
        }
    }

    /**
     * 获取表单元素
     * @param int $userId 会员ID
     * @return string 表单html
     */
    public function getField($userId='',$reg=true)
    {
        //字段列表
        $fieldList=$this->loadData();
        if(empty($fieldList)){
            return;
        }
        $typeField = api('duxcms','getFieldType');
        //获取内容信息
        if(!empty($userId)){
            $contentInfo=model('MemberData')->getInfo($userId);
        }
        $html='';
        $form=new DuxForm();
        foreach ($fieldList as $value) {
            if(!$reg){
                if (!$value['reg']) {
                    continue;
                }
            }
            $method=$typeField[$value['type']]['html'];
            $config=array();
            $config['title']=$value['name'];
            $config['name']=$value['field'];
            if($contentInfo[$value['field']]){
                $config['value']=$contentInfo[$value['field']];
            }else{
                $config['value']=$value['default'];
            }
            $config['size']=$value['size'];
            $config['datatype']=unserialize($value['datatype']);
            $config['tip']=$value['tip'];
            $config['errormsg']=$value['errormsg'];
            $list=explode(',', $value['config']);
            $newList=array();
            if(!empty($list)){
                foreach ($list as $key => $value) {
                    $newList[$key]['value']=$key+1;
                    $newList[$key]['title']=$value;
                }
            }
            $config['list']=$newList;
            $html.=$form->$method($config);
        }
        return $html;
    }

}