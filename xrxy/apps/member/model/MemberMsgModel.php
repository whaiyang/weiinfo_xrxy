<?php
/**
 * MemberMsgModel.php
 * 会员信息表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberMsgModel extends BaseModel
{
    protected $table = 'member_msg';
    /**
     * 获取列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->model
                    ->field('A.*,B.*,C.username as send_username,D.username as receive_username')
                    ->table('member_msg_has', 'A')
                    ->join('member_msg', 'B', array( 'A.msg_id', 'B.msg_id' ))
                    ->leftJoin('member', 'C', array( 'A.send_id', 'C.user_id' ))
                    ->join('member', 'D', array( 'A.receive_id', 'D.user_id' ))
                    ->where($condition)
                    ->order('A.has_id desc')
                    ->select();
    }
    /**
     * 获取总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->model
                    ->table('member_msg_has', 'A')
                    ->join('member_msg', 'B', array( 'A.msg_id', 'B.msg_id' ))
                    ->leftJoin('member', 'C', array( 'A.send_id', 'C.user_id' ))
                    ->join('member', 'D', array( 'A.receive_id', 'D.user_id' ))
                    ->where($condition)
                    ->count();
    }
    /**
     * 获取信息
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 信息
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 发送信息
     * @param array $data 信息
     * @return int ID
     */
    public function seadData($data , $system = 0)
    {
        $array = array();
        $array['title'] = html_in($data['title']);
        $array['content'] = html_in($data['content']);
        $array['time'] = time();
        $msgId = $this->insert($array);
        $arrayHas = array();
        $arrayHas['send_id'] = intval($data['send_id']);
        $arrayHas['receive_id'] = intval($data['receive_id']);
        $arrayHas['msg_id'] = $msgId;
        $arrayHas['send_system'] = $system;
        model('MemberMsgHas')->addData($arrayHas);
        return $msgId;
    }
}