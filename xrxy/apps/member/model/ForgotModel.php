<?php
/**
 * ForgotModel.php
 * 找回密码
 * @author Life <349865361@qq.com>
 * @version 20140309
 */
class ForgotModel extends BaseModel
{
    //发送密码找回邮件
    public function sendEmail($info){

        $config=appConfig('member');
        $config=$config['APP_SETTING'];
        //邮件内容
        $code=getcode(6);
        $url='http://'.$_SERVER["HTTP_HOST"].url('member/Login/index');
        $title = $info['username'].'您好，这是'.$config['SITE_TITLE'].'发送给您的密码找回邮件';
        $content = '尊敬的'.$info['username'].'，这是来自'.$config['SITE_TITLE'].'的密码找回邮件。系统已经将您的密码重置为：'.$code.'，请您登录后立即修改密码！ &lt;br/&gt;'.$config['SITE_TITLE'].' '.$url.'  &lt;br/&gt;'.date('Y-m-d H:i:s');
        $data=array();
        $data['user_id']=$info['user_id'];
        $data['password']=md5($code);
        model('Member')->saveData($data);
        //发送
        $mailArray=array();
        $mailArray['email']=$info['email'];
        $mailArray['title']=$title;
        $mailArray['content']=$content;
        $sysConfig=config('APP');
        $mailArray['safe_key']=$sysConfig['SAFE_KEY'];
        $mailUrl='http://'.$_SERVER["HTTP_HOST"].url('member/Email/send');
        $content=Http::doPost($mailUrl,$mailArray,5);
        
    }
}