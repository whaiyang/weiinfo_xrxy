<?php
/**
 * MemberMenuModel.php
 * 菜单数据管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class MemberMenuModel extends BaseModel
{
    /**
     * 获取菜单列表
     * @param string $key 可选主菜单KEY
     * @return array 菜单数组
     */
    public function getMenuList($purview = true, $userId = 0)
    {
        $data = $this->getMemberMenu();
        if(!$purview){
            return $data;
        }
        //获取用户组权限
        $userInfo = model('Member')->getRepeat('user_id', $userId);
        if(empty($userInfo)){
            return;
        }
        $userGroup = model('MemberGroup')->getInfo('group_id', $userInfo['group_id']);
        $menuUserPurview = unserialize($userGroup['menu_purview']);
        if(!empty($userInfo['role_id'])){
            $userRole = model('MemberRole')->getInfo('role_id', $userInfo['role_id']);
            $menuRolePurview = unserialize($userRole['menu_purview']);
        }
        $menuPurview = array_merge((array)$menuUserPurview,(array)$menuRolePurview);
        if (empty($menuPurview)) {
            return;
        }
        $menuList = array();
        foreach ($data as $topLabel => $top) {
            if (!in_array($top['name'], (array) $menuPurview)) {
                continue;
            }
            $menuList[$topLabel]['name'] = $top['name'];
            if (!empty($top['menu'])) {
                foreach ($top['menu'] as $parentLabel => $parent) {
                    //二级菜单
                    if (!in_array($parent['url'], (array) $menuPurview)) {
                        continue;
                    }
                    $menuList[$topLabel]['menu'][$parentLabel] = $parent;
                }
            }
        }
        //处理权限菜单接口
        $funList = hook_api('apiMemberMenuPurview');
        $data=array();
        if(!empty($funList)){
            foreach ($funList as $app => $value) {
                $menuList = api($app,'apiMemberMenuPurview',$menuList);
            }
        }
        return $menuList;

    }
    /**
     * 获取菜单数组
     * @param bool $cache 重建缓存
     * @return array 菜单数组
     */
    public function getMemberMenu($cache = false)
    {
        $cacheFile = ROOT_PATH . 'cache/app_cache/member_menu.php';
        $file = @file_get_contents($cacheFile);
        if (empty($file) || $cache) {
            $this->cacheMemberMenu();
            $file = @file_get_contents($cacheFile);
        }
        if (empty($file)) {
            return;
        }
        $list = unserialize($file);
        return array_filter($list);
    }
    /**
     * 缓存菜单数组
     */
    public function cacheMemberMenu()
    {
        $list = hook_api('apiMemberMenu');
        $dir = ROOT_PATH . 'cache/app_cache/';
        $cacheFile = $dir . 'member_menu.php';
        if (!file_exists($dir)) {
            if (!@mkdir($dir, true)) {
                throw new Exception($dir . ' 缓存目录无法创建，请检查目录权限！', 404);
            }
        }
        $menu = array();
        if(!empty($list)){
            //合并菜单
            foreach ($list as $appKey => $value) {
                $menu=array_merge_recursive((array)$menu,(array)$value);
            }
            //排序菜单
            foreach ((array) $menu as $topKey => $top) {
                if (!empty($menu[$topKey]['menu']) && is_array($menu[$topKey]['menu'])) {
                    $menu[$topKey]['menu']=array_order($top['menu'], 'order', 'asc');
                    foreach ((array) $menu[$topKey]['menu'] as $parentKey => $parent) {
                        if (!empty($parent['menu']) && is_array($parent['menu'])) {
                            $menu[$topKey]['menu'][$parentKey]['menu'] = array_order($parent['menu'], 'order', 'asc');
                        }
                    }
                }
            }
            $menu=array_order($menu, 'order', 'asc');
            $html = serialize($menu);
            if (!@file_put_contents($cacheFile, $html)) {
                throw new Exception('APP缓存文件无法写入，请检查目录权限！', 404);
            }
        }
    }
}