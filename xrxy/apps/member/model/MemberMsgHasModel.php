<?php
/**
 * MemberMsgHasModel.php
 * 会员信息关系表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberMsgHasModel extends BaseModel
{
    protected $table = 'member_msg_has';
    /**
     * 获取信息
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 信息
     */
    public function getInfo($name, $data)
    {
        return $this->find($name.'="' . $data .'"');
    }
    /**
     * 添加信息
     * @param array $data 信息
     * @return int ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 设置状态
     * @param array $data 信息
     * @return bool 状态
     */
    public function setData($where, $data)
    {
        return $this->update($where, $data);
    }
}