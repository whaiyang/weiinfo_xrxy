<?php
/**
 * MemberModel.php
 * 用户表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class MemberModel extends BaseModel
{
    protected $table = 'member';
    /**
     * 获取用户列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 用户列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->model->field('B.*,A.*,C.name as group_name,C.status as group_status,D.name as role_name')
                    ->table('member','A')
                    ->leftJoin('member_data', 'B', array( 'A.user_id', 'B.user_id' ))
                    ->join('member_group', 'C', array( 'A.group_id', 'C.group_id' ))
                    ->leftJoin('member_role', 'D', array( 'A.role_id', 'D.role_id' ))
                    ->where($condition)
                    ->order('A.user_id DESC')
                    ->limit($limit)
                    ->select();
    }
    /**
     * 获取用户总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->model
                    ->table('member','A')
                    ->leftJoin('member_data', 'B', array( 'A.user_id', 'B.user_id' ))
                    ->join('member_group', 'C', array( 'A.group_id', 'C.group_id' ))
                    ->where($condition)
                    ->count();
    }
    /**
     * 当前登录用户信息
     * @return array 用户信息
     */
    public function getCurrent()
    {
        $config = config('APP');
        $userId = get_cookie('user_uid');
        if(!empty($userId)){
            return $this->getInfo($userId);
        }else{
            return array();
        }
    }
    
    /**
     * 用户信息
     * @param int $userId 用户ID
     * @return array 用户信息
     */
    public function getInfo($userId)
    {
        $info = $this->model->field('B.*,A.*')
                    ->table('member','A')
                    ->leftJoin('member_data', 'B', array( 'A.user_id', 'B.user_id' ))
                    ->where('A.user_id=' . $userId)
                    ->find();
        //获取用户组权限
        $groupInfo = model('MemberGroup')->getInfo('group_id', $info['group_id']);
        $info['group_name'] = $groupInfo['name'];
        $info['group_status'] = $groupInfo['status'];
        $baseGroupPurview = unserialize($groupInfo['base_purview']);
        //获取角色权限
        if(!empty($info['role_id'])){
            $roleInfo = model('MemberRole')->getInfo('role_id', $info['role_id']);
            $baseRolePurview = unserialize($roleInfo['base_purview']);
        }
        $info['base_purview'] = array_merge((array)$baseGroupPurview,(array)$baseRolePurview);
        return $info;
    }
    /**
     * 查找用户
     * @param string $name 字段名
     * @param string $data 字段值
     * @return array 用户信息
     */
    public function getRepeat($name, $data, $userId = 0)
    {
        $where = '';
        if(!empty($userId)){
            $where = ' AND user_id<>'.$userId;
        }
        return $this->find($name.'="' . $data .'"'.$where);
    }
    /**
     * 添加用户信息
     * @param array $data 用户信息
     * @return int 用户ID
     */
    public function addData($data)
    {
        $data['reg_time'] = time();
        return $this->insert($data);
    }
    /**
     * 保存用户信息
     * @param array $data 用户信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('user_id=' . $data['user_id'], $data);
    }
    /**
     * 删除用户信息
     * @param int $userId 用户ID
     * @return bool 状态
     */
    public function delData($userId)
    {
        return $this->delete('user_id=' . $userId);
    }
    /**
     * 用户头像
     * @param int $userId 用户ID
     * @param int type 头像大小
     * @return string 头像地址
     */
    public function getAvatar($userId,$type=1)
    {
        $dir=ROOT_PATH.'/apps/member/avatar/';
        $url=__ROOT__.'/apps/member/avatar/';

        switch ($type) {
            case 0:
                $data = $userId.'/original.jpg';
                break;
            case 1:
                $data = $userId.'/small.jpg';
                break;
            case 2:
                $data = $userId.'/moderate.jpg';
                break;
            case 3:
                $data = $userId.'/large.jpg';
                break;
        }
        if(!file_exists($dir.$data)){
            $data = __PUBLICAPP__.'/images/user_avatar.png';
        }else{
            $data = $url.$data;
        }
        return $data;
    }

}