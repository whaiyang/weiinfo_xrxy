﻿<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<meta charset="utf-8">
<title>WeiQA</title>
<link href="<?=PUBDIR?>/css/style.css" rel="stylesheet" type="text/css"  />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function getArgs() { 
		var args = {};
        var query = location.search.substring(1);
         // Get query string
		var pairs = query.split("&"); 
                    // Break at ampersand
		for(var i = 0; i < pairs.length; i++) {
            var pos = pairs[i].indexOf('=');
             // Look for "name=value"
            if (pos == -1) continue;
                    // If not found, skip
                var argname = pairs[i].substring(0,pos);// Extract the name
                var value = pairs[i].substring(pos+1);// Extract the value
                //value = decodeURIComponent(value);// Decode it, if needed
                args[argname] = value;
                        // Store as a property
        }
		return args;// Return the object 
	}
	
	function link(e,url)
	{
		/*event = e||event;
		target = event.target||event.srcElement;
		url = target.getAttribute("link");
		if(url!=null)
			location.href = url;*/
		location.href = url;
	}
</script>
<script type="text/javascript">
	var $_GET = getArgs();
	var weiid = $_GET['weiid'];
	var keyword = $_GET['keyword'];
	var time = $_GET['time'];
	var category = $_GET['category'];
	var index = parseInt($_GET['index']);
	var count = parseInt($_GET['count']);
		
	$(document).ready(function()
	{
		$(".blog").each(function(){
			$(this).click(function(){
				location.href = $(this).attr("link");
			});
		});
		
		$("#clickToLoad").click(function()
		{
			$("#loadTxt").hide();
			$("#loadErr").hide();
			$("#loadImg").show();
			$.get("http://"+location.hostname+location.pathname+"?action=weixin&sub_action=WEB&target=loadVideos.php&weiid="+weiid+"&index="+index+"&count="+count+"&keyword="+keyword+"&time="+time+"&category="+category,
			function(data,status){
					$("#loadImg").hide();
					if(data.indexOf("NO_MORE")>-1)
					{
						$("#clickToLoad").html("没有更多了");
						$("#clickToLoad").attr({"disabled":"disabled"});
					}
					else
					{
						$("#logs").append(data);
						$("#loadTxt").show();
					}
			}).fail(function(){
				$("#loadImg").hide();
				$("#loadErr").show();
			});
			index += count;
		});
		$("#clickToLoad").click();
	});
</script>
</head>
<body>
    <div class="header">
        <div class="logo">
            <a href="index.html"><h1>WeiQA</h1></a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="content">
		<div id="logs">
		</div>		
        <div class="pagination">
			<button class="clickToLoad" id="clickToLoad"><img id="loadImg" style="height:30px;display:none;" src="images/load.gif"></img><span id="loadTxt">点击加载更多</span><span id="loadErr" style="display:none;">加载失败</span></button>
           <!-- <ul>
                <li><a href="#"><img src="images/right-arrow.png" alt=""></a></li>
                <li><a href="#">1</a></li>
                <li class="current"><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#"><img src="images/left-arrow.png" alt=""></a></li>
            </ul>-->
        </div>
    </div>
<div class="footer">
    <p><a href="#"></a></p>
</div>

</body>
</html>
