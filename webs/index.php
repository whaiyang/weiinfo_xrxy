<?php
	@$target = $_GET['target'];
	@$action = $_GET['controller'];
	if(isset($action))
	{
		//extend lib
		require_once("library/Smarty/Smarty.class.php");
		require_once("MC/Controller/Controller.class.php");
		require_once("MC/Model/Model.class.php");
		//init smarty
		$tpl = new Smarty();
		$tpl->template_dir = "webs";
		$tpl->compile_dir = "Cache/template/templateCompile";
		$tpl->config_dir = "Cache/template/templateConfig";
		$tpl->cache_dir = "Cache/template/templateCache";
		$tpl->php_handling = Smarty::PHP_ALLOW;

		$tpl->assign("CSS","Public/css");
		$tpl->assign("JS","Public/js");
		$tpl->assign("PLUG","Public/plug");
		$tpl->assign("IMG","Public/image");

		$action_dir = "MC/Controller/$action"."Controller.class.php";
		if(file_exists($action_dir))
		{
			include($action_dir);
			$class = $action."Controller";
			$class = new $class($tpl,$class,$action);
			$class-> __initialize();
			@$func = $_GET["func"];
			if(!empty($func))
				$class->$func();
			else
			{
				$class->index();
			}
		}
		else
			die("Action required doesn't exist");
	}
	else
		if(isset($target))
		{	
			if(!strpos($target,".php")&&!strpos($target,".html")&&!strpos($target,".htm"))
				$file_name = $target.".php";
			else
				$file_name = $target;
			include(OLDTPLDIR.$file_name);
		}