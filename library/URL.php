<?php
	
	function setURL($params,$action=null,$sub_action=null)
	{
		$str = "";
		$i = 0;
		if($action==null)
			$url = 'http://'.DOMAIN.PHPSELF.'?action=weixin&sub_action=WEB&';
		else
			$url = 'http://'.DOMAIN.PHPSELF.'?action='.$action.'&sub_action='.$sub_action.'&';
		foreach($params as $key=>$value)
		{
			$str .= $key.'='.$value;
			if($i<count($params)-1)
				$str .= "&";
			$i++;
		}
		return $url.$str;
	}

	function setController($controller,$func="index",$weiid="")
	{
		return setURL(array("controller"=>$controller,"func"=>$func,"weiid"=>$weiid));
	}

	function setCmsUrl($basename,$r,$arr)
	{
		$sec = explode("/",PHPSELF);
		if(APP_NAME=="")
			$url = 'http://'.DOMAIN."/".$sec[1]."/".$basename."/index.php".'?r='.$r;
		else
			$url = 'http://'.DOMAIN."/".$sec[1]."/".APP_NAME."/".$basename."/index.php".'?r='.$r;
		foreach($arr as $key=>$value)
		{
			$url .= "&$key=$value";
		}
		return $url;
	}

	function getCmsPic($url)
	{
		/*$sec = explode("/",PHPSELF);
		if(APP_NAME=="")
			$url = 'http://'.DOMAIN."/".$sec[1].$url;
		else
			$url = 'http://'.DOMAIN."/".$sec[1]."/".APP_NAME.$url;*/
		$url = 'http://'.DOMAIN.$url;
		return $url;
	}
	
	function getPicUrl($name)
	{
		if(APP_NAME=="")
			return "http://".URLDIR.'Public/image/'.$name;
		else
			return "http://".URLDIR.APP_NAME.'/Public/image/'.$name;
	}