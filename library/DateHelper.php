<?php
	class DateHelper
	{
		private static $ff = array("/时/","/时12/","/时24/","/分/","/秒/","/年/","/year2/","/年4/","/月/","/月EN/","/日/","/日EN/","/：/");
		private static $tf = array("H","h","H","i","s","Y","y","Y","m","M","d","D",":");
	
		public static function setLocation($loc='Asia/Chongqing')
		{
			date_default_timezone_set($loc);
		}
		
		public static function getSqlTimeDate()
		{
			return date('Y-m-d H:i:s');
		}
		
		public static function getTime($format)
		{
			$format = self::format($format);
			return date($format);
		}
		
		public static function getTimeFromUnix($format,$unix)
		{
			$format = self::format($format);
			return date($format,$unix);
		}
		
		public static function format($str)
		{
			return preg_replace(self::$ff,self::$tf,$str);
		}
	}