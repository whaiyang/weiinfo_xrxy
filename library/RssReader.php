﻿<?php
	require_once("library/lastRSS.php");
	
	class RssReader
	{
		private $rssObj = null;
		private $rssObjs = array();
		private $rss = null;
		
		public function __construct()
		{
			$this->rss=new lastRSS();
			$this->rss->cache_dir = 'Cache/rssCache'; 
			$this->rss->cache_time = 3600;
			$this->rss->default_cp = 'gb18030';
			$this->rss->cp = 'utf-8';
			$this->rss->items_limit = 0;
			$this->rss->date_format = 'U';
			$this->rss->stripHTML = false;
			$this->rss->CDATA = 'strip';//strip content nochange
		}
		
		//和获取单个RSS
		public function get($url,$charsetF=null,$charsetT="UTF-8")
		{
			/*$rss = file_get_contents($url);
			if($charsetF!=null)
				$rss = mb_convert_encoding($rss,"utf8","gb2312");//iconv($charsetF,$charsetT,$rss);//
			$this->rssObj = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);*/
			$data = $this->rss->Get($url);
			$this->rssObj = new RssAdapter($data);
			return $this->rssObj;
		}
		
		//获取一组rss
		public function getFArr($urls,$charsetF=null,$charsetT="UTF-8")
		{
			foreach($urls as $url)
			{
				/*$rss = file_get_contents($url);
				if($charsetF!=null)
					$rss = mb_convert_encoding($rss,$charsetT,$charsetF);
				$this->rssObjs[] = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);*/
				$data = $this->rss->Get($url);
				$this->rssObjs[] = new RssAdapter($data);
			}
			return $this->rssObjs;
		}
		
		//从已获取的一个rss中选择
		public function getMaxFObj($offset,$count)
		{
			$items = array();
			if($this->rssObj!=null)
			{
				$mlen = count($this->rssObj->channel->item);
				for($i=0;$i<$count&&$offset<$mlen;$i++,$offset++)
				{
					$items[] = $this->rssObj->channel->item[$offset];
				}
			}
			return $items;
		}
		
		//从已获取的一组rss中选择
		public function getMaxFArr($offset,$count)
		{
			$items = array();
			if(count($this->rssObjs)>0)
			{
				foreach($this->rssObjs as $rss)
				{
					$items[] = array();
					$curindex = count($items)-1;
					$mlen = count($rss->channel->item);
					for($i=0;$i<$count&&$offset<$mlen;$i++,$offset++)
					{
						$items[$curindex][] = $rss->channel->item[$offset];
					}
				}
			}
			return $items;
		}
	}
	
	class RssAdapter
	{
		public $encoding = "";
		public $title = "";
		public $link = "";
		public $description = "";
		public $language = "";
		public $lastBuildDate = "";
		public $image_title = "";
		public $image_url = "";
		public $image_link = "";
		public $image_width = "";
		public $image_height = "";
		public $channel = null;
		
		public function __construct($lastRssArr)
		{
			foreach($lastRssArr as $key=>$value)
				$this->$key = empty($value)?"":$value;
			$count = count($this->items);
			$item = array();
			for($i=0;$i<$count;$i++)
				$item[] = new RssItemAdapter($this->items[$i]);
			$this->items = null;
			$this->channel = new ChannelAdapter();
			$this->channel->item = $item;
		}
	}
	
	class ChannelAdapter
	{
		public $item = array();
	}
	
	class RssItemAdapter
	{
		public $title = "";
		public $link = "";
		public $description = "";
		public $category = "";
		public $pubDate = "";
		public $author = "";
		public $source = "";
		
		public function __construct($lastRssItemArr)
		{
			foreach($lastRssItemArr as $key=>$value)
				$this->$key = empty($value)?"":$value;
		}
	}