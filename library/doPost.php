<?php
	function do_post_request($url, $data, $method = "POST",$optional_headers = null)//OR GET
	{
		$params = array('http' => array(
					'method' => $method,
					'content' => $data
				));
		$headers = "";
		foreach($optional_headers as $key=>$value)
		{
			$headers .= $key.": ".$value."\r\n";
		}
		$headers .= "Content-Length:".strlen($data)."\r\n";
		$params['http']['header'] = $headers;
		$ctx = stream_context_create($params);
		$fp = @fopen($url, 'rb', false, $ctx);
		if (!$fp) {
			throw new Exception("Problem with $url, $php_errormsg");
		}
		$response = @stream_get_contents($fp);
		if ($response === false) {
			throw new Exception("Problem reading data from $url, $php_errormsg");
		}
		return $response;
	}