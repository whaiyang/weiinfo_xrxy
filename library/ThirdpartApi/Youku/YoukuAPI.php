<?php

	//category 资讯 拍客 纪录片 体育 汽车 科技 财经 娱乐 原创 游戏 搞笑 旅游 时尚 母婴 教育 排行
	//time today-今日 week-本周 month-本月 history-历史
	function Youku_searchVideo($keyword,$time="",$category="",$index=1,$count=5,$timeless=0,$timemore=0)
	{
		$url = HTTPS."://openapi.youku.com/v2/searches/video/by_keyword.json?";
		$params = array("client_id"=>Youku_APIKEY,"keyword"=>$keyword,"page"=>$index,"count"=>$count,"category"=>$category,
		"period"=>Youku_getTime($time),"orderby"=>"relevance","public_type"=>"all","paid"=>"0","timeless"=>$timeless,"timemore"=>$timemore,
		"streamtypes"=>"");
		foreach($params as $key=>$param)
			$url .= $key.'='.$param.'&';
		$json = file_get_contents($url);
		$obj = json_decode($json);
		return $obj;
	}
	
	function Youku_getTime($time)
	{
		switch($time)
		{
			case "":
				return "";
				break;
			case "今日":
				return "today";
				break;
			case "本周":
				return "week";
				break;
			case "本月":
				return "month";
				break;
			case "历史":
				return "history";
				break;
			default:
				return "";
		}
	}
	
	class YoukuVideo
	{
		private $obj;
		
		public function __construct($obj)
		{
			$this->obj = $obj;
		}
		
		public function getTotalNum()
		{
			return $this->obj->total;
		}
		
		public function getCount()
		{
			return count($this->obj->videos);
		}
		
		public function getIds()
		{
			$arr = array();
			foreach($this->obj->videos as $video)
			{
				$arr[] = $video->id; 
			}
			return $arr;
		}
		
		public function getTitles()
		{
			$arr = array();
			foreach($this->obj->videos as $video)
			{
				$arr[] = $video->title; 
			}
			return $arr;
		}
		
		public function getTimes()
		{
			$arr = array();
			foreach($this->obj->videos as $video)
			{
				$arr[] = $video->published; 
			}
			return $arr;
		}
		
		public static function getTime($obj)
		{
			return $obj->published;
		}
	
		public function getImages()
		{
			$arr = array();
			foreach($this->obj->videos as $video)
			{
				$arr[] = $video->thumbnail; 
			}
			return $arr;
		}
		
		public static function getImage($obj)
		{
			return $obj->thumbnail;
		}
		
		public function getFlashes()
		{
			$arr = array();
			foreach($this->obj->videos as $video)
			{
				$arr[] = "http://player.youku.com/player.php/Type/Folder/Fid/19492565/Ob/1/sid/".$video->id."/v.swf"; 
			}
			return $arr;
		}
		
		public static function getFlash($obj)
		{
			return "http://player.youku.com/player.php/Type/Folder/Fid/19492565/Ob/1/sid/".$obj->id."/v.swf";
		}
		
		public function getDurations($minu)//为 true 则返回分钟数 默认为秒
		{
			$arr = array();
			$ratio = 1;
			if($minu)
				$ratio = 1/60;
			foreach($this->obj->videos as $video)
			{
				$arr[] = number_format($video->duration*$ratio,0); 
			}
			return $arr;
		}
		
		public static function getDuration($obj,$minu)
		{
			$ratio = 1;
			if($minu)
				$ratio = 1/60;
			return number_format($obj->duration*$ratio,0);
		}
		
		public function index($index)
		{
			return $this->videos[$index];
		}
	}