<?php
	function Dou_excute_func($func,$params)
	{
		call_user_func($func,$params);
	}
	
	function Dou_gotAccessToken($params)
	{
		$result = $params[0];
		$obj = json_decode($result);
		$access_token = $obj->access_token;
		$expires_in = $obj->expires_in;
		$refresh_token = $obj->refresh_token;
		$douban_user_id = $obj->douban_user_id;
		@$error_code = $obj->code;
		if(!empty($error_code))//access_token过期
		{
			switch($error_code)
			{
				case DouError::$access_token_has_expire:
					Dou_refresh_token($refresh_token);
				break;
			}
		}
		SET_SESSION("access_token",$access_token);
		return;
	}
	
	function Dou_refresh_token($refresh_token)
	{
		$dou_uri = "https://www.douban.com/service/auth2/token";
		$client_id = Dou_APIKey;
		$client_secret = Dou_secret;
		$grant_type = "refresh_token";
		$request_data = 'client_id='.$client_id.'&client_secret='.$client_secret.
					   '&redirect_uri='.Dou_redirect_uri.'&grant_type='.$grant_type.'&refresh_token='.$refresh_token;
		$result = do_post_request($dou_uri,$request_data);
		excute_func("gotAccessToken",array($result));
	}